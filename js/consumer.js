
// Diccionario
const dict = {
    // Menu
    "Quienes somos": {
        pt: "Quem somos",
        en: "About Us",
        es: "Quienes somos"
    },
    "Servicios": {
        pt: "Servicios",
        en: "Services",
        es: "Servicios"
    },
    "Industrias": {
        pt: "Indústrias",
        en: "Industries",
        es: "Industrias"
    },
    "Sectores": {
        pt: "Sectores",
        en: "Sectors",
        es: "Sectores"
    },
    "Cobertura geográfica": {
        pt: "Cobertura geográfica",
        en: "Locations",
        es: "Cobertura geográfica"
    },
    "Contacto": {
        pt: "Fale conosco",
        en: "Contact Us",
        es: "Contacto"
    },
    // Sub menu
    "Agricultura / Salud Animal": {
        pt: "Agrucultura e Saúde Animal",
        en: "Agruculture and Animal Health",
        es: "Agricultura / Salud Animal"
    },
    "B2B / Industrial": {
        pt: "B2B / Industrial",
        en: "B2B / Industrial",
        es: "B2B / Industrial"
    },
    "Salud / Farma": {
        pt: "Saúde / Farma",
        en: "Healthcare / Pharma",
        es: "Salud / Farma"
    },
    "Consumo": {
        pt: "Consumo",
        en: "Consumer",
        es: "Consumo"
    },
    "Desarrollo de negocios": {
        pt: "Desenvolvimiento de Negócios",
        en: "Business Development",
        es: "Desarrollo de negocios"
    },
    // Quienes somos
    "QUIÉNES <br> SOMOS": {
        pt: "QUEM SOMOS",
        en: "ABOUT US",
        es: "QUIÉNES SOMOS"
    },
    "abou_us_copy": {
        pt: "Há mais de 10 anos oferecemos serviços de pesquisa de mercado qualitativa e quantitativa na América Latina.",
        en: "For more than 10 years, we have been providing qualitative and quantitative market research services in Latin America.",
        es: "Hace más de 10 años proveemos servicios de investigación de mercado cualitativa y cuantitativa en Latinoamérica."
    },
    "Desarrollamos proyectos para una amplia diversidad de empresas dando como resultado la acumulación de experiencia en diversos sectores.": {
        pt: "Desenvolvemos projetos para uma ampla gama de empresas, tendo como resultado a acumulação de experiência em diversos setores.",
        en: "Our company develops projects for a wide range of companies, resulting in the accumulation of experience in various market sectors.",
        es: "Desarrollamos proyectos para una amplia diversidad de empresas dando como resultado la acumulación de experiencia en diversos sectores."
    },
    "Conducimos una variedad de estudios incluyendo: segmentación de mercado, test de concepto y mensaje, evaluación y pronóstico de mercado, estudios de precio, posicionamiento de marca, satisfacción / lealtad de clientes, estudios de usos y actitudes, voz del consumidor, entre otros.": {
        pt: "Conduzimos uma variedade de estudos incluindo: segmentação do mercado, teste de conceito e mensagem, avaliação e projeção de mercado, estudos de preço, posicionamento da marca, satisfação / lealdade dos clientes, estudos de uso e atitude, voz do consumidor, entre outros.",
        en: "We conduct a variety of studies including: market segmentation, concept and message testing, market assessment and forecasting, pricing research, brand positioning, customer satisfaction / loyalty, customer tracking (attitude and usage), voice of the consumer, among others.",
        es: "Conducimos una variedad de estudios incluyendo: segmentación de mercado, test de concepto y mensaje, evaluación y pronóstico de mercado, estudios de precio, posicionamiento de marca, satisfacción / lealtad de clientes, estudios de usos y actitudes, voz del consumidor, entre otros."
    },
    "Nos distinguen los altos estándares de calidad, puntualidad, privacidad, y cumplimiento.": {
        pt: "Nos diferenciamos pelos altos níveis de qualidade, pontualidade, privacidade e cumprimento.",
        en: "Koobet stands out through our high standards of quality, punctuality, privacy, and compliance.",
        es: "Nos distinguen los altos estándares de calidad, puntualidad, privacidad, y cumplimiento."
    },
    // Nuestros servicios
    "NUESTROS <br> SERVICIOS": {
        pt: "Nossos <br> Serviços",
        en: "OUR SERVICES",
        es: "NUESTROS <br> SERVICIOS"
    },
    // subtitulo
    "Mediante abordajes cualitativos y cuantitativos conducimos una variedad de estudios, incluyendo:": {
        pt: "Mediante abordagens qualitativas e quantitativas conduzimos uma variedade de estudos, incluindo:",
        en: "Through qualitative and quantitative approaches, we conduct a variety of studies, including:",
        es: "Mediante abordajes cualitativos y cuantitativos conducimos una variedad de estudios, incluyendo:"
    },
    "Satisfacción de clientes": {
        pt: "Pesquisa de satisfação/lealdade dos clientes",
        en: "Customer satisfaction",
        es: "Satisfacción de clientes"
    },
    "Segmentación de mercados": {
        pt: "Segmentação de mercados",
        en: "Market segmentation",
        es: "Segmentación de mercados"
    },
    "Dimensionamiento del mercado y Market Share": {
        pt: "Dimensionamento do mercado e Market Share",
        en: "Market sizing & Market Share",
        es: "Dimensionamiento del mercado y Market Share"
    },
    "Elasticidad y sensibilidad de precios": {
        pt: "Elasticidade e sensibilidade de preços",
        en: "Price elasticity",
        es: "Elasticidad y sensibilidad de precios"
    },
    "Test de conceptos y productos": {
        pt: "Teste de conceitos e produtos",
        en: "Concept and product testing",
        es: "Test de conceptos y productos"
    },
    "Imagen y valor de marcas": {
        pt: "Imagem e valor de marcas",
        en: "Brand equity & image",
        es: "Imagen y valor de marcas"
    },
    "Comportamiento del consumidor": {
        pt: "Comportamento do consumidor",
        en: "Consumer behaviour",
        es: "Comportamiento del consumidor"
    },
    "Investigación publicitaria": {
        pt: "Pesquisa publicitária ",
        en: "Advertising research",
        es: "Investigación publicitaria"
    },
    // Tipos de estudio y metodologia
    "TIPOS DE ESTUDIO Y METODOLOGÍAS": {
        pt: "TIPOS DE ESTUDOS E METODOLOGIAS",
        en: "Types of studies & methodologies",
        es: "TIPOS DE ESTUDIO Y METODOLOGÍAS"
    },
    "Según el tipo de estudio podemos usar alguna o una combinación de las siguientes metodologías:": {
        pt: "Segundo o tipo de estudo podemos usar alguma ou uma combinação das seguintes metodologias:",
        en: "Depending on the type of study, we can use some or a combination of the following methodologies:",
        es: "Según el tipo de estudio podemos usar alguna o una combinación de las siguientes metodologías:"
    },
    "Investigación cualitativa": {
        pt: "Pesquisa qualitativa",
        en: "Qualitative research",
        es: "Investigación cualitativa"
    },
    "Entrevistas en profundidad": {
        pt: "Entrevistas em profundidade pessoais",
        en: "Face-to-face in-depth interviews",
        es: "Entrevistas en profundidad"
    },
    "Entrevistas en profundidad telefónicas": {
        pt: "Entrevistas em profundidade telefônicas",
        en: "Telephone in-depth interviews",
        es: "Entrevistas en profundidad telefónicas"
    },
    "Entrevistas etnográficas": {
        pt: "Entrevistas etnográficas",
        en: "Ethnographic interviews",
        es: "Entrevistas etnográficas"
    },
    "Focus groups": {
        pt: "Focus groups",
        en: "Focus groups",
        es: "Focus groups"
    },
    "Desk Research": {
        pt: "Desk Research",
        en: "Desk Research",
        es: "Desk Research"
    },
   //
    "Mystery ": {
        pt: "Mystery Shopping",
        en: "Mystery Shopping",
        es: "Mystery "
    },
    //
    "Entrevistas personales": {
        pt: "Entrevistas pessoais (PAPI, CAPI)",
        en: "Face-to-face interviews (PAPI, CAPI)",
        es: "Entrevistas personales"
    },
    //
    "Entrevistas telefónicas (CATI)": {
        pt: "Entrevistas telefônicas (CATI)",
        en: "Telephone interviews (CATI)",
        es: "Entrevistas telefónicas (CATI)"
    },
    //
    "Encuestas Online": {
        pt: "Entrevistas online",
        en: "Online surveys",
        es: "Encuestas Online"
    },
    //
    "Investigación cuantitativa": {
        pt: "Pesquisa quantitativa",
        en: "Quantitative research",
        es: "Investigación cuantitativa"
    },
    "INDUSTRIAS CUBIERTAS": {
        pt: "Indústrias atendidAs",
        en: "INDUSTRIES WE SERVED",
        es: "INDUSTRIAS CUBIERTAS"
    },
    "Automotriz": {
        pt: "Automotiva",
        en: "Automotive",
        es: "Automotriz"
    },
    "Bienes y servicios de consumo": {
        pt: "Bens e serviços de consumo",
        en: "Consumer goods and services",
        es: "Bienes y servicios de consumo"
    },
    "Construcción": {
        pt: "Construção",
        en: "Construction",
        es: "Construcción"
    },
    "Entretenimiento": {
        pt: "Entretenimento",
        en: "Entertainment",
        es: "Entretenimiento"
    },
    "Servicios financieros": {
        pt: "Serviços financeiros",
        en: "Financial Services",
        es: "Servicios financieros"
    },
    "Tecnología de la información": {
        pt: "Tecnologia da informação",
        en: "IT",
        es: "Tecnología de la información"
    },
    "Retail": {
        pt: "Retail",
        en: "Retail",
        es: "Retail"
    },
    "Telecomunicaciones": {
        pt: "Telecomunicações",
        en: "Telecommunication",
        es: "Telecomunicaciones"
    },
    "Utilities": {
        pt: "Utilities",
        en: "Utilities",
        es: "Utilities"
    },
    // Areas terapeuticas
    "ÁREAS TERAPÉUTICAS": {
        pt: "Áreas terapêuticas",
        en: "Therapeutic areas",
        es: "ÁREAS TERAPÉUTICAS"
    },
    "Anestesiología": {
        pt: "Anestesiologia",
        en: "Anesthesiology",
        es: "Anestesiología"
    },
    "Cardiología": {
        pt: "Cardiologia",
        en: "Cardiology",
        es: "Cardiología"
    },
    "Cirugía ortopédica y traumatológica": {
        pt: "Cirurgia ortopédica e traumatológica",
        en: "Trauma and Orthopedic Surgery",
        es: "Cirugía ortopédica y traumatológica"
    },
    "Cuidado intensivo": {
        pt: "Medicina intensiva",
        en: "Intensive Care",
        es: "Cuidado intensivo"
    },
    "Dermatología": {
        pt: "Dermatologia",
        en: "Dermatology",
        es: "Dermatología"
    },
    "Dispositivos médicos & Tecnología": {
        pt: "Dispositivos médicos & Tecnologia",
        en: "Medical devices & Technology ",
        es: "Dispositivos médicos & Tecnología"
    },
    "Endocrinología": {
        pt: "Endocrinologia",
        en: "Endocrinologist",
        es: "Endocrinología"
    },
    "Enfermedades infecciosas": {
        pt: "Doenças infecciosas",
        en: "Infectious Diseases",
        es: "Enfermedades infecciosas"
    },
    "Gastroenterología": {
        pt: "Gastrenterologia",
        en: "Gastroenterology",
        es: "Gastroenterología"
    },
    "Medicina reproductive": {
        pt: "Medicina reprodutiva",
        en: "Reproductive Medicine",
        es: "Medicina reproductive"
    },
    "Neumonología": {
        pt: "Pneumologia",
        en: "Pulmonology",
        es: "Neumonología"
    },
    "Neurología": {
        pt: "Neurologia",
        en: "Neurology ",
        es: "Neurología"
    },
    "Obstetricia / Ginecología": {
        pt: "Obstetrícia / Ginecologia",
        en: "Obstetrics / Gynecology",
        es: "Obstetricia / Ginecología"
    },
    "Oftalmología": {
        pt: "Oftalmologia",
        en: "Ophtamology",
        es: "Oftalmología"
    },
    "Oncología": {
        pt: "Oncologia",
        en: "Oncology",
        es: "Oncología"
    },
    "Pediatría": {
        pt: "Pediatria",
        en: "Pediatrics",
        es: "Pediatría"
    },
    "Radiología": {
        pt: "Radiologia",
        en: "Radiology",
        es: "Radiología"
    },
    "Tratamiento del dolor": {
        pt: "Tratamento de dor",
        en: "Pain Management",
        es: "Tratamiento del dolor"
    },
    "Urología": {
        pt: "Radiologia",
        en: "Urology",
        es: "Urología"
    },
    // Paises
    "PAÍSES<br>DONDE OPERAMOS": {
        pt: "PAÍSES<br>ONDE OPERAMOS",
        en: "Countries <br> where we operate",
        es: "PAÍSES<br>DONDE OPERAMOS"
    },
    "Argentina": {
        pt: "Argentina",
        en: "Argentina",
        es: "Argentina"
    },
    "Bolivia": {
        pt: "Bolivia",
        en: "Bolivia",
        es: "Bolivia"
    },
    "Brasil": {
        pt: "Brasil",
        en: "Brazil",
        es: "Brasil"
    },
    "Chile": {
        pt: "Chile",
        en: "Chile",
        es: "Chile"
    },
    "Colombia": {
        pt: "Colômbia",
        en: "Colombia",
        es: "Colombia"
    },
    "Costa Rica": {
        pt: "Costa Rica",
        en: "",
        es: "Costa Rica"
    },
    "Ecuador": {
        pt: "Ecuador",
        en: "Ecuador",
        es: "Ecuador"
    },
    "Guatemala": {
        pt: "Guatemala",
        en: "Guatemala",
        es: "Guatemala"
    },
    "Honduras": {
        pt: "Honduras",
        en: "Honduras",
        es: "Honduras"
    },
    "México": {
        pt: "México",
        en: "Mexico",
        es: "México"
    },
    "Nicaragua": {
        pt: "Nicaragua",
        en: "Nicaragua",
        es: "Nicaragua"
    },
    "Panamá": {
        pt: "Panamá",
        en: "Panama",
        es: "Panamá"
    },
    "Paraguay": {
        pt: "Paraguai",
        en: "Paraguay",
        es: "Paraguay"
    },
    "Perú": {
        pt: "Perú",
        en: "Peru",
        es: "Perú"
    },
    "República Dominicana": {
        pt: "Rep. Dominicana",
        en: "Dominican Republic",
        es: "República Dominicana"
    },
    "Uruguay": {
        pt: "Uruguai",
        en: "Uruguay",
        es: "Uruguay"
    },
    // Formulario de contacto
    "CONTÁCTENOS": {
        pt: "Fale conosco",
        en: "Contact Us",
        es: "CONTÁCTENOS"
    },
    "Ayúdenos a comprender sus necesidades y cómo podríamos servirle.": {
        pt: "Nos ajude a compreender suas necesidades e como podemos serví-lo.",
        en: "Please help us understand your needs and how we can best help you",
        es: "Ayúdenos a comprender sus necesidades y cómo podríamos servirle."
    },
    "Nombre:": {
        pt: "Nome:",
        en: "First name:",
        es: "Nombre:"
    },
    "Apellido:": {
        pt: "Sobrenome:",
        en: "Last name:",
        es: "Apellido:"
    },
    "Empresa:": {
        pt: "Empresa:",
        en: "Company:",
        es: "Empresa:"
    },
    "País:": {
        pt: "País:",
        en: "Country:",
        es: "País:"
    },
    "Email:": {
        pt: "Email:",
        en: "Email:",
        es: "Email:"
    },
    "¿Cómo podemos ayudarlo?:": {
        pt: "Como podemos te ajudar?:",
        en: "How can we help you?:",
        es: "¿Cómo podemos ayudarlo?:"
    },
    "Enviar": {
        pt: "Enviar",
        en: "Submit",
        es: "Enviar"
    },
};

$(function () {
    // Inicializacion de variables
    let english = $('#en');
    let portuguese = $('#pt');
    let spanish = $('#es');

    // Ingles
    english.on('click', function (e) {
        e.preventDefault();
        let translator = $('body').translate({lang: "en", t: dict});
        localStorage.setItem('lang', 'en');

        // Obtener traducciones
        let name = translator.get("Nombre:");
        let apellido = translator.get("Apellido:");
        let empresa = translator.get("Empresa:");
        let pais = translator.get("País:");
        let email = translator.get("Email:");
        let ayuda = translator.get("¿Cómo podemos ayudarlo?:");

        // Reemplazar en el form
        $('#inputName').attr('placeholder', name);
        $('#inputLastname').attr('placeholder', apellido);
        $('#inputCompany').attr('placeholder', empresa);
        $('#inputCountry').attr('placeholder', pais);
        $('#inputEmail').attr('placeholder', email);
        $('#validationTextarea').attr('placeholder', ayuda);
    });

    // Spanish
    spanish.on('click', function (e) {
        e.preventDefault();
        let translator = $('body').translate({lang: "es", t: dict});
        localStorage.setItem('lang', 'es');

        // Obtener traducciones
        let name = translator.get("Nombre:");
        let apellido = translator.get("Apellido:");
        let empresa = translator.get("Empresa:");
        let pais = translator.get("País:");
        let email = translator.get("Email:");
        let ayuda = translator.get("¿Cómo podemos ayudarlo?:");

        // Reemplazar en el form
        $('#inputName').attr('placeholder', name);
        $('#inputLastname').attr('placeholder', apellido);
        $('#inputCompany').attr('placeholder', empresa);
        $('#inputCountry').attr('placeholder', pais);
        $('#inputEmail').attr('placeholder', email);
        $('#validationTextarea').attr('placeholder', ayuda);
    });

    // Portugues
    portuguese.on('click', function (e) {
        e.preventDefault();
        let translator = $('body').translate({lang: "pt", t: dict});
        localStorage.setItem('lang', 'pt');

        // Obtener traducciones
        let name = translator.get("Nombre:");
        let apellido = translator.get("Apellido:");
        let empresa = translator.get("Empresa:");
        let pais = translator.get("País:");
        let email = translator.get("Email:");
        let ayuda = translator.get("¿Cómo podemos ayudarlo?:");

        // Reemplazar en el form
        $('#inputName').attr('placeholder', name);
        $('#inputLastname').attr('placeholder', apellido);
        $('#inputCompany').attr('placeholder', empresa);
        $('#inputCountry').attr('placeholder', pais);
        $('#inputEmail').attr('placeholder', email);
        $('#validationTextarea').attr('placeholder', ayuda);
    });
    // Set defualt language
    const lang = localStorage.getItem('lang');
    if (lang === "en"){
        $( "#en" ).trigger( "click" );
    }
    if (lang === "es"){
        $( "#es" ).trigger( "click" );
    }
    if (lang === "pt"){
        $( "#pt" ).trigger( "click" );
    }
    if ( lang == null){
        $( "#en" ).trigger( "click" );
    }

});
