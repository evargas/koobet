
// Diccionario
const dict = {
    // Menu
    "Quienes somos": {
        pt: "Quem somos",
        en: "About Us",
        es: "Quienes somos"
    },
    "Servicios": {
        pt: "Servicios",
        en: "Services",
        es: "Servicios"
    },
    "Sectores": {
        pt: "Sectores",
        en: "Sectors",
        es: "Sectores"
    },
    "Cobertura geográfica": {
        pt: "Cobertura geográfica",
        en: "Locations",
        es: "Cobertura geográfica"
    },
    "Contacto": {
        pt: "Fale conosco",
        en: "Contact Us",
        es: "Contacto"
    },
    // Sub menu
    "Agricultura": {
        pt: "Agrucultura e Saúde Animal",
        en: "Agruculture and Animal Health",
        es: "Agricultura"
    },
    "B2B / Industrial": {
        pt: "B2B / Industrial",
        en: "B2B / Industrial",
        es: "B2B / Industrial"
    },
    "Salud / Farma": {
        pt: "Saúde / Farma",
        en: "Healthcare / Pharma",
        es: "Salud / Farma"
    },
    "Consumo": {
        pt: "Consumo",
        en: "Consumer",
        es: "Consumo"
    },
    "Desarrollo de negocios": {
        pt: "Desenvolvimiento de Negócios",
        en: "Business Development",
        es: "Desarrollo de negocios"
    },
    // Quienes somos
    "QUIÉNES <br> SOMOS": {
        pt: "QUEM <br> SOMOS",
        en: "ABOUT US",
        es: "QUIÉNES <br> SOMOS"
    },
    "Koobet ofrece servicios de investigación cualitativa y cuantitativa para las industrias de Salud y Farmacéutica.": {
        pt: "Koobet oferece serviços de pesquisa qualitativa e quantitativa para as indústrias de Saúde e Farmacêutica.",
        en: "Koobet offers qualitative and quantitative research services for the Healthcare and Pharmaceutical industries.",
        es: "Koobet ofrece servicios de investigación cualitativa y cuantitativa para las industrias de Salud y Farmacéutica."
    },
    "Conducimos una variedad de estudios incluyendo: segmentación de mercado, test de concepto y mensaje, evaluación y pronóstico de mercado, estudios de precios, posicionamiento de marca, satisfacción/lealtad de clientes, Usos y actitudes, voz del consumidor, entre otros.": {
        pt: "Conduzimos uma variedade de estudos, incluindo: segmentação de mercado, teste de conceitos e mensagem, avaliação e projeção de mercado, estudos de preço, posicionamento da marca, satisfação / lealdade de clientes, estudos de uso e atitude, voz do consumidor, entre outros.",
        en: "We conduct a variety of studies including: market segmentation, concept and message testing, market assessment and forecasting, pricing research, brand positioning, customer satisfaction / loyalty, customer tracking (attitude and usage), and voice of the customer, among others.",
        es: "Conducimos una variedad de estudios incluyendo: segmentación de mercado, test de concepto y mensaje, evaluación y pronóstico de mercado, estudios de precios, posicionamiento de marca, satisfacción/lealtad de clientes, Usos y actitudes, voz del consumidor, entre otros."
    },
    "Utilizando distintas metodologías como encuestas online, encuestas telefónicas, entrevistas en profundidad y focus groups. ": {
        pt: "Utilizando diferentes metodologias como pesquisas online, pesquisas telefônicas, entrevistas em profundidade e focus groups.",
        en: "We use different methodologies such as online interviews, telephone interviews, in-depth interviews and focus groups.",
        es: "Utilizando distintas metodologías como encuestas online, encuestas telefónicas, entrevistas en profundidad y focus groups. "
    },
    "Accediendo a todos los tipos de grupos de interés, incluyendo:": {
        pt: "Alcançando a todos os tipos de grupos de interesse, incluindo:",
        en: "Our company has access to all types of interest groups, including:",
        es: "Accediendo a todos los tipos de grupos de interés, incluyendo:"
    },
    "Médicos especialistas": {
        pt: "Médicos especialistas",
        en: "Expert physicians",
        es: "Médicos especialistas"
    },
    "accesses_and_payers": {
        pt: "Especialistas em acesso ao mercado e pagadores",
        en: "Experts in market accesses and payers",
        es: "Expertos en acceso al mercado y payers "
    },
    "Líderes de opinión": {
        pt: "Líderes de opinião",
        en: "Key Opinion Leader",
        es: "Líderes de opinión"
    },
    "Farmacéuticos": {
        pt: "Farmacêuticos",
        en: "Pharmacists",
        es: "Farmacéuticos"
    },
    "Enfermeras": {
        pt: "Enfermeiras",
        en: "Nurses",
        es: "Enfermeras"
    },
    "Pacientes": {
        pt: "Pacientes",
        en: "Patients ",
        es: "Pacientes"
    },
    "Hospital (directores y compradores)": {
        pt: "Hospital (diretores e compradores)",
        en: "Hospital (directors and buyers)",
        es: "Hospital (directores y compradores)"
    },
    // Nuestros servicios
    "NUESTROS <br> SERVICIOS": {
        pt: "Nossos <br> Serviços",
        en: "OUR SERVICES",
        es: "NUESTROS <br> SERVICIOS"
    },
    // subtitulo
    "Mediante abordajes cualitativos y cuantitativos conducimos una variedad de estudios, incluyendo:": {
        pt: "Mediante abordagens qualitativas e quantitativas conduzimos uma variedade de estudos, incluindo:",
        en: "Through qualitative and quantitative approaches, we conduct a variety of studies, including:",
        es: "Mediante abordajes cualitativos y cuantitativos conducimos una variedad de estudios, incluyendo:"
    },
    "Segmentación de mercado": {
        pt: "Segmentação de mercado",
        en: "Market segmentation",
        es: "Segmentación de mercado"
    },
    "Test de concepto y mensaje": {
        pt: "Teste de conceito e mensagem",
        en: "Concept testing",
        es: "Test de concepto y mensaje"
    },
    "Evaluación y pronóstico de mercado": {
        pt: "Avaliação e projeção de mercado",
        en: "Market assessment and forecasting",
        es: "Evaluación y pronóstico de mercado"
    },
    "Estudios de precios": {
        pt: "Estudos de preço",
        en: "Pricing research",
        es: "Estudios de precios"
    },
    "Posicionamiento de la marca": {
        pt: "Posicionamento da marca",
        en: "Brand positioning",
        es: "Posicionamiento de la marca"
    },
    "Satisfacción/lealtad de clientes": {
        pt: "Satisfação/lealdade de clientes",
        en: "Customer satisfaction/ loyalty",
        es: "Satisfacción/lealtad de clientes"
    },
    "Estudios de usos y actitudes": {
        pt: "Estudos de uso e atitude",
        en: "Customer tracking (attitude and usage)",
        es: "Estudios de usos y actitudes"
    },
    "Voz del consumidor": {
        pt: "Voz do consumidor",
        en: "Voice of the customer",
        es: "Voz del consumidor"
    },
    // subtitulo
    "TIPOS DE RESPONDENTES ALCANZADOS": {
        pt: "Voice of the customer",
        en: "TYPES OF RESPONDENTS REACHED",
        es: "TIPOS DE RESPONDENTES ALCANZADOS"
    },
    "Consumidor": {
        pt: "Consumidor",
        en: "Consumer",
        es: "Consumidor"
    },
    // Seccion 2
    "METODOLOGÍAS": {
        pt: "METODOLOGIAS",
        en: "Methodologies",
        es: "METODOLOGÍAS"
    },
    "Según el tipo de estudio podemos usar alguna o una combinación de las siguientes metodologías:": {
        pt: "Segundo o tipo de estudo podemos usar alguma ou uma combinação das seguintes metodologias:",
        en: "Depending on the type of study, we can use some or a combination of the following methodologies:",
        es: "Según el tipo de estudio podemos usar alguna o una combinación de las siguientes metodologías:"
    },
    "Investigación cualitativa": {
        pt: "Pesquisa qualitativa",
        en: "Qualitative research",
        es: "Investigación cualitativa"
    },
    "Entrevistas en profundidad personales": {
        pt: "Entrevistas em profundidade pessoais",
        en: "Face-to-face in-depth interviews",
        es: "Entrevistas en profundidad personales"
    },
    "Entrevistas en profundidad telefónicas": {
        pt: "Entrevistas em profundidade telefônicas",
        en: "Telephone in-depth interviews",
        es: "Entrevistas en profundidad telefónicas"
    },
    "Focus groups": {
        pt: "Focus groups",
        en: "Focus groups",
        es: "Focus groups"
    },
    "Desk Research": {
        pt: "Desk Research",
        en: "Desk Research",
        es: "Desk Research"
    },
    "Mystery ": {
        pt: "Mystery Shopping",
        en: "Mystery Shopping",
        es: "Mystery "
    },
    "Entrevistas personales": {
        pt: "Entrevistas pessoais (PAPI, CAPI)",
        en: "Face-to-face interviews (PAPI, CAPI)",
        es: "Entrevistas personales"
    },
    "Entrevistas telefónicas (CATI)": {
        pt: "Entrevistas telefônicas (CATI)",
        en: "Telephone interviews (CATI)",
        es: "Entrevistas telefónicas (CATI)"
    },
    "Encuestas Online": {
        pt: "Entrevistas online",
        en: "Online surveys",
        es: "Encuestas Online"
    },
    "Investigación cuantitativa": {
        pt: "Pesquisa quantitativa",
        en: "Quantitative research",
        es: "Investigación cuantitativa"
    },
    // Areas terapeuticas
    "ÁREAS TERAPÉUTICAS": {
        pt: "Áreas terapêuticas",
        en: "Therapeutic areas",
        es: "ÁREAS TERAPÉUTICAS"
    },
    "Áreas terapéuticas": {
        pt: "Áreas terapêuticas",
        en: "Therapeutic areas",
        es: "ÁREAS TERAPÉUTICAS"
    },
    "Anestesiología": {
        pt: "Anestesiologia",
        en: "Anesthesiology",
        es: "Anestesiología"
    },
    "Cardiología": {
        pt: "Cardiologia",
        en: "Cardiology",
        es: "Cardiología"
    },
    "Cirugía ortopédica y traumatológica": {
        pt: "Cirurgia ortopédica e traumatológica",
        en: "Trauma and Orthopedic Surgery",
        es: "Cirugía ortopédica y traumatológica"
    },
    "Cuidado intensivo": {
        pt: "Medicina intensiva",
        en: "Intensive Care",
        es: "Cuidado intensivo"
    },
    "Dermatología": {
        pt: "Dermatologia",
        en: "Dermatology",
        es: "Dermatología"
    },
    "Dispositivos médicos y Tecnología": {
        pt: "Dispositivos médicos & Tecnologia",
        en: "Medical devices and Technology",
        es: "Dispositivos médicos & Tecnología"
    },
    "Endocrinología": {
        pt: "Endocrinologia",
        en: "Endocrinologist",
        es: "Endocrinología"
    },
    "Enfermedades infecciosas": {
        pt: "Doenças infecciosas",
        en: "Infectious Diseases",
        es: "Enfermedades infecciosas"
    },
    "Gastroenterología": {
        pt: "Gastrenterologia",
        en: "Gastroenterology",
        es: "Gastroenterología"
    },
    "Medicina reproductive": {
        pt: "Medicina reprodutiva",
        en: "Reproductive Medicine",
        es: "Medicina reproductive"
    },
    "Neumonología": {
        pt: "Pneumologia",
        en: "Pulmonology",
        es: "Neumonología"
    },
    "Neurología": {
        pt: "Neurologia",
        en: "Neurology ",
        es: "Neurología"
    },
    "Obstetricia / Ginecología": {
        pt: "Obstetrícia / Ginecologia",
        en: "Obstetrics / Gynecology",
        es: "Obstetricia / Ginecología"
    },
    "Oftalmología": {
        pt: "Oftalmologia",
        en: "Ophtamology",
        es: "Oftalmología"
    },
    "Oncología": {
        pt: "Oncologia",
        en: "Oncology",
        es: "Oncología"
    },
    "Pediatría": {
        pt: "Pediatria",
        en: "Pediatrics",
        es: "Pediatría"
    },
    "Radiología": {
        pt: "Radiologia",
        en: "Radiology",
        es: "Radiología"
    },
    "Tratamiento del dolor": {
        pt: "Tratamento de dor",
        en: "Pain Management",
        es: "Tratamiento del dolor"
    },
    "Urología": {
        pt: "Radiologia",
        en: "Urology",
        es: "Urología"
    },
    // Paises
    "PAÍSES<br>DONDE OPERAMOS": {
        pt: "PAÍSES<br>ONDE OPERAMOS",
        en: "Countries <br> where we operate",
        es: "PAÍSES<br>DONDE OPERAMOS"
    },
    "Argentina": {
        pt: "Argentina",
        en: "Argentina",
        es: "Argentina"
    },
    "Bolivia": {
        pt: "Bolivia",
        en: "Bolivia",
        es: "Bolivia"
    },
    "Brasil": {
        pt: "Brasil",
        en: "Brazil",
        es: "Brasil"
    },
    "Chile": {
        pt: "Chile",
        en: "Chile",
        es: "Chile"
    },
    "Colombia": {
        pt: "Colômbia",
        en: "Colombia",
        es: "Colombia"
    },
    "Costa Rica": {
        pt: "Costa Rica",
        en: "",
        es: "Costa Rica"
    },
    "Ecuador": {
        pt: "Ecuador",
        en: "Ecuador",
        es: "Ecuador"
    },
    "Guatemala": {
        pt: "Guatemala",
        en: "Guatemala",
        es: "Guatemala"
    },
    "Honduras": {
        pt: "Honduras",
        en: "Honduras",
        es: "Honduras"
    },
    "México": {
        pt: "México",
        en: "Mexico",
        es: "México"
    },
    "Nicaragua": {
        pt: "Nicaragua",
        en: "Nicaragua",
        es: "Nicaragua"
    },
    "Panamá": {
        pt: "Panamá",
        en: "Panama",
        es: "Panamá"
    },
    "Paraguay": {
        pt: "Paraguai",
        en: "Paraguay",
        es: "Paraguay"
    },
    "Perú": {
        pt: "Perú",
        en: "Peru",
        es: "Perú"
    },
    "República Dominicana": {
        pt: "Rep. Dominicana",
        en: "Dominican Republic",
        es: "República Dominicana"
    },
    "Uruguay": {
        pt: "Uruguai",
        en: "Uruguay",
        es: "Uruguay"
    },
    // Formulario de contacto
    "CONTÁCTENOS": {
        pt: "Fale conosco",
        en: "Contact Us",
        es: "CONTÁCTENOS"
    },
    "Ayúdenos a comprender sus necesidades y cómo podríamos servirle.": {
        pt: "Nos ajude a compreender suas necesidades e como podemos serví-lo.",
        en: "Please help us understand your needs and how we can best help you",
        es: "Ayúdenos a comprender sus necesidades y cómo podríamos servirle."
    },
    "Nombre:": {
        pt: "Nome:",
        en: "First name:",
        es: "Nombre:"
    },
    "Apellido:": {
        pt: "Sobrenome:",
        en: "Last name:",
        es: "Apellido:"
    },
    "Empresa:": {
        pt: "Empresa:",
        en: "Company:",
        es: "Empresa:"
    },
    "País:": {
        pt: "País:",
        en: "Country:",
        es: "País:"
    },
    "Email:": {
        pt: "Email:",
        en: "Email:",
        es: "Email:"
    },
    "¿Cómo podemos ayudarlo?:": {
        pt: "Como podemos te ajudar?:",
        en: "How can we help you?:",
        es: "¿Cómo podemos ayudarlo?:"
    },
    "Enviar": {
        pt: "Enviar",
        en: "Submit",
        es: "Enviar"
    },
};

$(function () {
    // Inicializacion de variables
    let english = $('#en');
    let portuguese = $('#pt');
    let spanish = $('#es');

    // Ingles
    english.on('click', function (e) {
        e.preventDefault();
        let translator = $('body').translate({lang: "en", t: dict});
        localStorage.setItem('lang', 'en');

        // Obtener traducciones
        let name = translator.get("Nombre:");
        let apellido = translator.get("Apellido:");
        let empresa = translator.get("Empresa:");
        let pais = translator.get("País:");
        let email = translator.get("Email:");
        let ayuda = translator.get("¿Cómo podemos ayudarlo?:");

        // Reemplazar en el form
        $('#inputName').attr('placeholder', name);
        $('#inputLastname').attr('placeholder', apellido);
        $('#inputCompany').attr('placeholder', empresa);
        $('#inputCountry').attr('placeholder', pais);
        $('#inputEmail').attr('placeholder', email);
        $('#validationTextarea').attr('placeholder', ayuda);
    });

    // Spanish
    spanish.on('click', function (e) {
        e.preventDefault();
        let translator = $('body').translate({lang: "es", t: dict});
        localStorage.setItem('lang', 'es');

        // Obtener traducciones
        let name = translator.get("Nombre:");
        let apellido = translator.get("Apellido:");
        let empresa = translator.get("Empresa:");
        let pais = translator.get("País:");
        let email = translator.get("Email:");
        let ayuda = translator.get("¿Cómo podemos ayudarlo?:");

        // Reemplazar en el form
        $('#inputName').attr('placeholder', name);
        $('#inputLastname').attr('placeholder', apellido);
        $('#inputCompany').attr('placeholder', empresa);
        $('#inputCountry').attr('placeholder', pais);
        $('#inputEmail').attr('placeholder', email);
        $('#validationTextarea').attr('placeholder', ayuda);
    });

    // Portugues
    portuguese.on('click', function (e) {
        e.preventDefault();
        let translator = $('body').translate({lang: "pt", t: dict});
        localStorage.setItem('lang', 'pt');

        // Obtener traducciones
        let name = translator.get("Nombre:");
        let apellido = translator.get("Apellido:");
        let empresa = translator.get("Empresa:");
        let pais = translator.get("País:");
        let email = translator.get("Email:");
        let ayuda = translator.get("¿Cómo podemos ayudarlo?:");

        // Reemplazar en el form
        $('#inputName').attr('placeholder', name);
        $('#inputLastname').attr('placeholder', apellido);
        $('#inputCompany').attr('placeholder', empresa);
        $('#inputCountry').attr('placeholder', pais);
        $('#inputEmail').attr('placeholder', email);
        $('#validationTextarea').attr('placeholder', ayuda);
    });
    // Set defualt language
    const lang = localStorage.getItem('lang');
    if (lang === "en"){
        $( "#en" ).trigger( "click" );
    }
    if (lang === "es"){
        $( "#es" ).trigger( "click" );
    }
    if (lang === "pt"){
        $( "#pt" ).trigger( "click" );
    }
    if ( lang == null){
        $( "#en" ).trigger( "click" );
    }
});
