// Diccionario
const dict = {
    // Menu
    "Agricultura / Salud Animal": {
        pt: "Agrucultura / Saúde Animal",
        en: "Agriculture / Animal Health",
        es: "Agricultura / Salud Animal"
    },
    "B2B / Industrial": {
        pt: "B2B / Industrial",
        en: "B2B / Industrial",
        es: "B2B / Industrial"
    },
    "Salud / Farma": {
        pt: "Saúde / Farma",
        en: "Healthcare / Pharma",
        es: "Salud / Farma"
    },
    "Consumo": {
        pt: "Consumo",
        en: "Consumer",
        es: "Consumo"
    },
    "Desarrollo de negocios": {
        pt: "Desenvolvimiento de Negócios",
        en: "Business Development",
        es: "Desarrollo de negocios"
    },
    // Menu desplegable
    "Quienes somos": {
        pt: "Quem somos",
        en: "About Us",
        es: "Quienes somos"
    },
    "Cobertura geográfica": {
        pt: "",
        en: "Geographical coverage",
        es: "Cobertura geográfica"
    },
    "Contacto": {
        pt: "Fale conosco",
        en: "Contact Us",
        es: "Contacto"
    },
    // Seccion 1
    "Market Research & Business Development": {
        pt: "Market Research & Business Development",
        en: "Market Research & Business Development",
        es: "Market Research & Business Development"
    },
    "COMPRENDIENDO Y CONECTANDO": {
        pt: "COMPRENDENDO E CONECTANDO",
        en: "UNDERSTANDING AND CONNECTING",
        es: "COMPRENDIENDO Y CONECTANDO"
    },
    " CON SU AUDIENCIA OBJETIVO": {
        pt: "COM SEU PÚBLICO ALVO",
        en: "WITH YOUR TARGET AUDIENCE",
        es: " CON SU AUDIENCIA OBJETIVO"
    },
    // Quienes somos
    "QUIÉNES <br> SOMOS": {
        pt: "QUEM <br> SOMOS",
        en: "ABOUT US",
        es: "QUIÉNES <br> SOMOS"
    },
    "about_us": {
        pt: "Somos especialistas em pesquisa de mercado e desenvolvimento de negócios. Nossos clientes nos escolhem por nossa experiência, compromisso e resultados.",
        en: "We are experts in market research and business development. Our clients choose us for our experience, commitment and results.",
        es: "Somos expertos en investigación de mercados y desarrollo de negocios. Nuestros clientes nos eligen por nuestra experiencia, compromiso y resultados."
    },
    // Cobertura geo
    "COBERTURA <br>GEOGRÁFICA": {
        pt: "COBERTURA <br>GEOGRÁFICO",
        en: "GEOGRAPHICAL <br>COVERAGE",
        es: "COBERTURA <br>GEOGRÁFICA"
    },
    "geographical_coverage": {
        pt: "Equipes especializadas em cada país da América Latina, conhecimento do mercado local, fluidez no idioma e acesso às melhores fontes de informação para atender às suas necessidades.",
        en: "Specialized teams in each Latin American country with knowledge of the local market, fluency in the language and access to the best sources of information to meet your needs.",
        es: "Equipo especializado en cada país de Latinoamérica, conocimiento del mercado local, fluidez del idioma y el acceso a las mejores fuentes de información para responder a sus necesidades."
    },
    // Formulario de contacto
    "CONTÁCTENOS": {
        pt: "Fale conosco",
        en: "Contact Us",
        es: "CONTÁCTENOS"
    },
    "Ayúdenos a comprender sus necesidades y cómo podríamos servirle.": {
        pt: "Nos ajude a compreender suas necesidades e como podemos serví-lo.",
        en: "Please help us understand your needs and how we can best help you",
        es: "Ayúdenos a comprender sus necesidades y cómo podríamos servirle."
    },
    "Nombre:": {
        pt: "Nome:",
        en: "First name:",
        es: "Nombre:"
    },
    "Apellido:": {
        pt: "Sobrenome:",
        en: "Last name:",
        es: "Apellido:"
    },
    "Empresa:": {
        pt: "Empresa:",
        en: "Company:",
        es: "Empresa:"
    },
    "País:": {
        pt: "País:",
        en: "Country:",
        es: "País:"
    },
    "Email:": {
        pt: "Email:",
        en: "Email:",
        es: "Email:"
    },
    "¿Cómo podemos ayudarlo?:": {
        pt: "Como podemos te ajudar?:",
        en: "How can we help you?:",
        es: "¿Cómo podemos ayudarlo?:"
    },
    "Enviar": {
        pt: "Enviar",
        en: "Submit",
        es: "Enviar"
    },
};

$(function () {
    // Inicializacion de variables
    let english = $('#en');
    let portuguese = $('#pt');
    let spanish = $('#es');

    // Ingles
    english.on('click', function (e) {
        e.preventDefault();
        let translator = $('body').translate({lang: "en", t: dict});
        localStorage.setItem('lang', 'en');

        // Obtener traducciones
        let name = translator.get("Nombre:");
        let apellido = translator.get("Apellido:");
        let empresa = translator.get("Empresa:");
        let pais = translator.get("País:");
        let email = translator.get("Email:");
        let ayuda = translator.get("¿Cómo podemos ayudarlo?:");

        // Reemplazar en el form
        $('#inputName').attr('placeholder', name);
        $('#inputLastname').attr('placeholder', apellido);
        $('#inputCompany').attr('placeholder', empresa);
        $('#inputCountry').attr('placeholder', pais);
        $('#inputEmail').attr('placeholder', email);
        $('#validationTextarea').attr('placeholder', ayuda);
    });

    // Spanish
    spanish.on('click', function (e) {
        e.preventDefault();
        let translator = $('body').translate({lang: "es", t: dict});
        localStorage.setItem('lang', 'es');

        // Obtener traducciones
        let name = translator.get("Nombre:");
        let apellido = translator.get("Apellido:");
        let empresa = translator.get("Empresa:");
        let pais = translator.get("País:");
        let email = translator.get("Email:");
        let ayuda = translator.get("¿Cómo podemos ayudarlo?:");

        // Reemplazar en el form
        $('#inputName').attr('placeholder', name);
        $('#inputLastname').attr('placeholder', apellido);
        $('#inputCompany').attr('placeholder', empresa);
        $('#inputCountry').attr('placeholder', pais);
        $('#inputEmail').attr('placeholder', email);
        $('#validationTextarea').attr('placeholder', ayuda);
    });

    // Portugues
    portuguese.on('click', function (e) {
        e.preventDefault();
        let translator = $('body').translate({lang: "pt", t: dict});
        localStorage.setItem('lang', 'pt');

        // Obtener traducciones
        let name = translator.get("Nombre:");
        let apellido = translator.get("Apellido:");
        let empresa = translator.get("Empresa:");
        let pais = translator.get("País:");
        let email = translator.get("Email:");
        let ayuda = translator.get("¿Cómo podemos ayudarlo?:");

        // Reemplazar en el form
        $('#inputName').attr('placeholder', name);
        $('#inputLastname').attr('placeholder', apellido);
        $('#inputCompany').attr('placeholder', empresa);
        $('#inputCountry').attr('placeholder', pais);
        $('#inputEmail').attr('placeholder', email);
        $('#validationTextarea').attr('placeholder', ayuda);
    });
    // Set defualt language
    const lang = localStorage.getItem('lang');
    if (lang === "en"){
        $( "#en" ).trigger( "click" );
    }
    if (lang === "es"){
        $( "#es" ).trigger( "click" );
    }
    if (lang === "pt"){
        $( "#pt" ).trigger( "click" );
    }
    if ( lang == null){
        $( "#en" ).trigger( "click" );
    }
});
