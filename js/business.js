
// Diccionario
const dict = {
    // Menu
    "Quienes somos": {
        pt: "Quem somos",
        en: "About Us",
        es: "Quienes somos"
    },
    "Servicios": {
        pt: "Servicios",
        en: "Services",
        es: "Servicios"
    },
    "Sectores": {
        pt: "Sectores",
        en: "Sectors",
        es: "Sectores"
    },
    "Cobertura geográfica": {
        pt: "Cobertura geográfica",
        en: "Locations",
        es: "Cobertura geográfica"
    },
    "Contacto": {
        pt: "Fale conosco",
        en: "Contact Us",
        es: "Contacto"
    },
    // Sub menu
    "Agricultura / Salud Animal": {
        pt: "Agrucultura e Saúde Animal",
        en: "Agruculture and Animal Health",
        es: "Agricultura / Salud Animal"
    },
    "B2B / Industrial": {
        pt: "B2B / Industrial",
        en: "B2B / Industrial",
        es: "B2B / Industrial"
    },
    "Salud / Farma": {
        pt: "Saúde / Farma",
        en: "Healthcare / Pharma",
        es: "Salud / Farma"
    },
    "Consumo": {
        pt: "Consumo",
        en: "Consumer",
        es: "Consumo"
    },
    "Desarrollo de negocios": {
        pt: "Desenvolvimiento de Negócios",
        en: "Business Development",
        es: "Desarrollo de negocios"
    },
    // Seccion 1
    "Market Research & Business Development": {
        pt: "Market Research & Business Development",
        en: "Market Research & Business Development",
        es: "Market Research & Business Development"
    },
    "COMPRENDIENDO Y CONECTANDO": {
        pt: "COMPRENDENDO E CONECTANDO",
        en: "UNDERSTANDING AND CONNECTING",
        es: "COMPRENDIENDO Y CONECTANDO"
    },
    " CON SU AUDIENCIA OBJETIVO": {
        pt: "COM SEU PÚBLICO ALVO",
        en: "WITH YOUR TARGET AUDIENCE",
        es: " CON SU AUDIENCIA OBJETIVO"
    },
    // Quienes somos
    "QUIÉNES <br> SOMOS": {
        pt: "QUEM SOMOS",
        en: "ABOUT US",
        es: "QUIÉNES SOMOS"
    },
    "about_us_copy": {
        pt: "Há mais de 10 anos ajudando empresas de diferentes setores a se expandirem na América Latina, com serviço de desenvolvimento de negócios e pesquisa de mercado. ",
        en: "For more than 10 years, we have been helping companies from different market sectors to expand into Latin America, with business development and market research services.",
        es: "Hace más de 10 años asistimos a empresas de distintos sectores a expandirse en Latinoamérica, con servicios de desarrollo de negocios e investigación de mercados."
    },
    "De acuerdo al perfil buscado, identificamos a potenciales importadores, distribuidores, o socios y los conectamos con nuestros clientes para que puedan hacer negocios.": {
        pt: "De acordo com o perfil buscado, identificamos os potenciais importadores, distribuidores ou sócios e os conectamos com nossos clientes para que possam fazer negócios.",
        en: "In order to match the desired profile, we identify potential importers, distributors or partners and connect them to our customers to begin the process of doing business.",
        es: "De acuerdo al perfil buscado, identificamos a potenciales importadores, distribuidores, o socios y los conectamos con nuestros clientes para que puedan hacer negocios."
    },
    "Para nuestros clientes que buscan dimensionar el mercado, detectar posibles barreras u oportunidades, conducimos estudios entrevistando a distintos tipos de respondentes como fabricantes, distribuidores, competidores, expertos, cámaras y asociaciones industriales.": {
        pt: "Para nossos clientes que buscam dimensionar o mercado, detectar possíveis barreiras ou oportunidades, conduzimos estudos entrevistando fabricantes, distribuidores, competidores, especialistas, câmaras e associações industriais.",
        en: "For our clients who are looking to identify the size of the market, detect possible barriers or seek potential opportunities, we conduct studies interviewing manufacturers, distributors, competitors, specialists, government agencies and industrial associations.",
        es: "Para nuestros clientes que buscan dimensionar el mercado, detectar posibles barreras u oportunidades, conducimos estudios entrevistando a distintos tipos de respondentes como fabricantes, distribuidores, competidores, expertos, cámaras y asociaciones industriales."
    },
    "Operamos en Argentina, Bolivia, Brasil, Chile, Colombia, Costa Rica, Cuba, Ecuador, El Salvador, Guatemala, Honduras, México, Nicaragua, Panamá, Paraguay, Perú, República Dominicana y Uruguay.": {
        pt: "Operamos na Argentina, Brasil, Bolívia, Chile, Colômbia, Costa Rica, Cuba, Equador, El Salvador, Guatemala, Honduras, México, Nicarágua, Panamá, Paraguai, Peru, Rep. Dominicana e Uruguai.",
        en: "We operate in Argentina, Brazil, Bolivia, Chile, Colombia, Costa Rica, Cuba, Dominican Rep. Ecuador, El Salvador, Guatemala, Honduras, Mexico, Nicaragua, Panama, Paraguay, Peru and Uruguay.",
        es: "Operamos en Argentina, Bolivia, Brasil, Chile, Colombia, Costa Rica, Cuba, Ecuador, El Salvador, Guatemala, Honduras, México, Nicaragua, Panamá, Paraguay, Perú, República Dominicana y Uruguay."
    },
    // Servicios
    "SERVICIOS": {
        pt: "Serviços",
        en: "SERVICES",
        es: "SERVICIOS"
    },
    // subtitulo
    "Búsqueda de Distribuidores/ representantes/ consumidores finales:": {
        pt: "Busca de Distribuidores/ representantes/ consumidores finais:",
        en: "Distributor / Representative / End User Search:",
        es: "Búsqueda de Distribuidores/ representantes/ consumidores finales:"
    },
    "Selección de potenciales socios o clientes para sus productos o servicios.": {
        pt: "Seleção de potenciais sócios ou clientes para seus produtos ou serviços.",
        en: " Selection of potential partners or customers for your products or services.",
        es: "Selección de potenciales socios o clientes para sus productos o servicios."
    },
    "Introducción de su empresa y productos o servicios a decisores de compra para determinar el interés.": {
        pt: "Introdução de sua empresa, produtos ou serviços aos decisores de compra para determinar o interesse.",
        en: " Introducing your company and its products or services to purchasing decision makers to determine interest.",
        es: "Introducción de su empresa y productos o servicios a decisores de compra para determinar el interés."
    },
    "Identificación de potenciales candidatos.": {
        pt: "Identificação de potenciais candidatos.",
        en: " Identification of potential candidates.",
        es: "Identificación de potenciales candidatos."
    },
    "Reporte con los perfiles de las empresas e información de contacto.": {
        pt: "Relatório com os perfis das empresas e informação de contato.",
        en: " Report with company profiles and contact information.",
        es: "Reporte con los perfiles de las empresas e información de contacto."
    },
    "Feedback de los empresas sobre sus productos o servicios.": {
        pt: "Feedback das empresas sobre seus produtos ou serviços.",
        en: " Feedback from companies about your products or services.",
        es: "Feedback de los empresas sobre sus productos o servicios."
    },
    "Resumen del mercado para su producto o servicio.": {
        pt: "Resumo do mercado para seu produto ou serviço.",
        en: " Market summary for your product or service.",
        es: "Resumen del mercado para su producto o servicio."
    },
    // subtitulo
    "Organización de una visita al mercado:": {
        pt: "Organização de uma visita de campo:",
        en: "Organizing market visits:",
        es: "Organización de una visita al mercado:"
    },
    "Selección de potenciales socios o clientes para sus productos o servicios.": {
        pt: "Seleção de potenciais sócios ou clientes para seus produtos ou serviços.",
        en: "Selection of potential partners or customers for your products or services.",
        es: "Selección de potenciales socios o clientes para sus productos o servicios."
    },
    "Introducción de su empresa y productos o servicios a decisores de compra para determinar el interés.": {
        pt: "Introdução de sua empresa, produtos ou serviços aos decisores de compra para determinar o interesse.",
        en: "Introducing your company, products or services to purchasing decision makers to determine interest.",
        es: "Introducción de su empresa y productos o servicios a decisores de compra para determinar el interés."
    },
    "Organización de una agenda de reuniones con potenciales candidatos.": {
        pt: "Organização de uma agenda de reuniões com potenciais candidatos.",
        en: "Organizing a meeting agenda with potential candidates.",
        es: "Organización de una agenda de reuniones con potenciales candidatos."
    },
    "Perfiles de las empresas e información de contacto.": {
        pt: "Perfis das empresas e informação de contato.",
        en: "Company profiles and contact information.",
        es: "Perfiles de las empresas e información de contacto."
    },
    "Soporte logístico.": {
        pt: "Suporte logístico.",
        en: "Logistic support.",
        es: "Soporte logístico."
    },
    // subtitulo
    "Representación en el mercado:": {
        pt: "Representação no mercado:",
        en: "IN - Market representation:",
        es: "Representación en el mercado:"
    },
    "Selección de potenciales clientes.": {
        pt: "Seleção de potenciais clientes.",
        en: "Selection of potential customers.",
        es: "Selección de potenciales clientes."
    },
    "Introducción de su empresa y productos o servicios a decisores de compra.": {
        pt: "Introdução da sua empresa, produtos ou serviços aos decisores de compra.",
        en: "Introducing your company, products or services to purchasing decision makers. ",
        es: "Introducción de su empresa y productos o servicios a decisores de compra."
    },
    "Seguimientos para determinar el interés.": {
        pt: "Acompanhamentos para determinar o interesse.",
        en: "Follow-ups to determine interest.",
        es: "Seguimientos para determinar el interés."
    },
    "report_containing_the_following_information": {
        pt: "Relatório contendo as seguintes informações:",
        en: "Report containing the following information:",
        es: "Reporte conteniendo la siguiente información:"
    },
    "Listado de empresas contactadas.": {
        pt: "Lista de empresas contatadas.",
        en: "List of companies contacted.",
        es: "Listado de empresas contactadas."
    },
    "Perfiles de los candidatos interesados e información de contacto.": {
        pt: "Perfis dos candidatos interessados e informação de contato.",
        en: "Customer profiles and contact information.",
        es: "Perfiles de los candidatos interesados e información de contacto."
    },
    "Feedback de las empresas sobre su oferta.": {
        pt: "Feedback das empresas sobre sua oferta.",
        en: "Companies feedback on your offer.",
        es: "Feedback de las empresas sobre su oferta."
    },
    "Reporte con la lista de empresas contactadas, status y feedback.": {
        pt: "Relatório com a lista de empresas contatadas, status e feedback.",
        en: "Report with the list of companies contacted, status and feedbacks. ",
        es: "Reporte con la lista de empresas contactadas, status y feedback."
    },
    "Organización de una agenda de reuniones para un representante de su empresa con potenciales clientes.": {
        pt: "Organização de uma agenda de reuniões para um representante de sua empresa com potenciais clientes.",
        en: "Organize a meeting schedule for a representative of your company with potential customers.",
        es: "Organización de una agenda de reuniones para un representante de su empresa con potenciales clientes."
    },
    "Reuniones de nuestro consultor con potenciales clientes.": {
        pt: "Reuniões do nosso consultor com potenciais clientes.",
        en: "Meetings with our consultant and potential customers.",
        es: "Reuniones de nuestro consultor con potenciales clientes."
    },
    // subtitulo
    "Investigación de mercado ad hoc:": {
        pt: "Pesquisa de mercado ad hoc:",
        en: "Ad hoc market research:",
        es: "Investigación de mercado ad hoc:"
    },
    "Tamaño del mercado y crecimiento.": {
        pt: "Tamanho do mercado e crescimento.",
        en: "Market Sizing and Growth.",
        es: "Tamaño del mercado y crecimiento."
    },
    "Proyecciones y tendencias.": {
        pt: "Projeções e tendências.",
        en: "Forecasting and Trends.",
        es: "Proyecciones y tendencias."
    },
    "Análisis de la cadena de valor.": {
        pt: "Análise da cadeia de valor.",
        en: "Value Chain Analysis.",
        es: "Análisis de la cadena de valor."
    },
    "Análisis de competidores.": {
        pt: "Análise da competidores.",
        en: "Competitor Analysis.",
        es: "Análisis de competidores."
    },
    "Investigación de precios.": {
        pt: "Pesquisa de preço.",
        en: "Pricing Research.",
        es: "Investigación de precios."
    },
    "Canales de distribución.": {
        pt: "Canais de distribuição.",
        en: "Distribution Channels.",
        es: "Canales de distribución."
    },
    "Drivers y barreras.": {
        pt: "Drivers e barreiras.",
        en: "Drivers and Barriers.",
        es: "Drivers y barreras."
    },
    "Perfiles de clientes.": {
        pt: "Perfis de clientes.",
        en: "Customer Profiling.",
        es: "Perfiles de clientes."
    },
    "Comportamiento del consumidor.": {
        pt: "",
        en: "Customer Behaviour.",
        es: "Comportamiento del consumidor."
    },
    // Seccion 2
    "SECTORES<br>ATENDIDOS": {
        pt: "SECTORES ATENDIDOS",
        en: "SECTORS SERVED",
        es: "SECTORES ATENDIDOS"
    },
    "Aeronáutica": {
        pt: "Aeronáutica",
        en: "Aerospace",
        es: "Aeronáutica"
    },
    "Agropecuario": {
        pt: "Agropecuário",
        en: "Agriculture ",
        es: "Agropecuario"
    },
    "Automotriz": {
        pt: "Automotivo",
        en: "Automotive",
        es: "Automotriz"
    },
    "Biotecnología": {
        pt: "Biotecnologia ",
        en: "Biotechnology",
        es: "Biotecnología"
    },
    "Química": {
        pt: "Quimica ",
        en: "Chemical ",
        es: "Química"
    },
    "Construcción": {
        pt: "Construção ",
        en: "Construction ",
        es: "Construcción"
    },
    "Cosmética y Belleza": {
        pt: "Cosmética e Beleza",
        en: "Cosmetics and Beauty  ",
        es: "Cosmética y Belleza"
    },
    "Educación": {
        pt: "Educação",
        en: "Education ",
        es: "Educación"
    },
    "Eléctrico": {
        pt: "Elétrico",
        en: "Electric",
        es: "Eléctrico"
    },
    "Electrónica": {
        pt: "Eletrônica",
        en: "Electronics",
        es: "Electrónica"
    },
    "Energía": {
        pt: "Energia",
        en: "Energy ",
        es: "Energía"
    },
    "Entretenimiento": {
        pt: "Entretenimento",
        en: "Entertainment ",
        es: "Entretenimiento"
    },
    "Farmacéutico": {
        pt: "Farmacêutico",
        en: "",
        es: "Farmacéutico"
    },
    "Financiero": {
        pt: "Financeiro",
        en: "Finances",
        es: "Financiero"
    },
    "Franquicias": {
        pt: "Franquias ",
        en: "Franchises",
        es: "Franquicias"
    },
    "Gobierno": {
        pt: "Governo ",
        en: "Goverment",
    es: "Gobierno"
    },
    "Herramientas": {
        pt: "Hotelaria ",
        en: "Haardware",
        es: "Herramientas"
    },
    "Hotelería": {
        pt: "Ferramentas",
        en: "Hotel",
        es: "Hotelería"
    },
    "Iluminación": {
        pt: "Iluminação ",
        en: "Lightning",
        es: "Iluminación"
    },
    "Ingredientes": {
        pt: "Ingredientes",
        en: "Ingredients",
        es: "Ingredientes"
    },
    "Medio Ambiente": {
        pt: "Meio Ambiente",
        en: "Enviromental",
        es: "Medio Ambiente"
    },
    "Minería y Metales": {
        pt: "Mineração e Metais",
        en: "Mining and Metals",
        es: "Minería y Metales"
    },
    "Packaging": {
        pt: "Embalagem ",
        en: "Packaging",
        es: "Packaging"
    },
    "Petróleo y Gas": {
        pt: "Petróleo e Gás",
        en: "Oil and Gas",
        es: "Petróleo y Gas"
    },
    "Retail": {
        pt: "Retail",
        en: "Retail",
        es: "Retail"
    },
    "Salud": {
        pt: "Saúde ",
        en: "Healthcare",
        es: "Salud"
    },
    "Seguridad": {
        pt: "Segurança",
        en: "Safety and Security",
        es: "Seguridad"
    },
    "Servicios Públicos": {
        pt: "Serviços Públicos",
        en: "Public Services",
        es: "Servicios Públicos"
    },
    "Tecnología": {
        pt: "Tecnologia ",
        en: "Technology",
        es: "Tecnología"
    },
    "Textil": {
        pt: "Têxtil ",
        en: "Textiles",
        es: "Textil"
    },
    "Transporte": {
        pt: "Transporte ",
        en: "Transportation",
        es: "Transporte"
    },
    // Seccion 3
    "PAÍSES<br>DONDE OPERAMOS": {
        pt: "PAÍSES<br>ONDE OPERAMOS",
        en: "Countries <br> where we operate",
        es: "PAÍSES<br>DONDE OPERAMOS"
    },
    "Argentina": {
        pt: "Argentina",
        en: "Argentina",
        es: "Argentina"
    },
    "Bolivia": {
        pt: "Bolivia",
        en: "Bolivia",
        es: "Bolivia"
    },
    "Brasil": {
        pt: "Brasil",
        en: "Brazil",
        es: "Brasil"
    },
    "Chile": {
        pt: "Chile",
        en: "Chile",
        es: "Chile"
    },
    "Colombia": {
        pt: "Colômbia",
        en: "Colombia",
        es: "Colombia"
    },
    "Costa Rica": {
        pt: "Costa Rica",
        en: "",
        es: "Costa Rica"
    },
    "Ecuador": {
        pt: "Ecuador",
        en: "Ecuador",
        es: "Ecuador"
    },
    "Guatemala": {
        pt: "Guatemala",
        en: "Guatemala",
        es: "Guatemala"
    },
    "Honduras": {
        pt: "Honduras",
        en: "Honduras",
        es: "Honduras"
    },
    "México": {
        pt: "México",
        en: "Mexico",
        es: "México"
    },
    "Nicaragua": {
        pt: "Nicaragua",
        en: "Nicaragua",
        es: "Nicaragua"
    },
    "Panamá": {
        pt: "Panamá",
        en: "Panama",
        es: "Panamá"
    },
    "Paraguay": {
        pt: "Paraguai",
        en: "Paraguay",
        es: "Paraguay"
    },
    "Perú": {
        pt: "Perú",
        en: "Peru",
        es: "Perú"
    },
    "República Dominicana": {
        pt: "Rep. Dominicana",
        en: "Dominican Republic",
        es: "República Dominicana"
    },
    "Uruguay": {
        pt: "Uruguai",
        en: "Uruguay",
        es: "Uruguay"
    },
    // Formulario de contacto
    "CONTÁCTENOS": {
        pt: "Fale conosco",
        en: "Contact Us",
        es: "CONTÁCTENOS"
    },
    "Ayúdenos a comprender sus necesidades y cómo podríamos servirle.": {
        pt: "Nos ajude a compreender suas necesidades e como podemos serví-lo.",
        en: "Please help us understand your needs and how we can best help you",
        es: "Ayúdenos a comprender sus necesidades y cómo podríamos servirle."
    },
    "Nombre:": {
        pt: "Nome:",
        en: "First name:",
        es: "Nombre:"
    },
    "Apellido:": {
        pt: "Sobrenome:",
        en: "Last name:",
        es: "Apellido:"
    },
    "Empresa:": {
        pt: "Empresa:",
        en: "Company:",
        es: "Empresa:"
    },
    "País:": {
        pt: "País:",
        en: "Country:",
        es: "País:"
    },
    "Email:": {
        pt: "Email:",
        en: "Email:",
        es: "Email:"
    },
    "¿Cómo podemos ayudarlo?:": {
        pt: "Como podemos te ajudar?:",
        en: "How can we help you?:",
        es: "¿Cómo podemos ayudarlo?:"
    },
    "Enviar": {
        pt: "Enviar",
        en: "Submit",
        es: "Enviar"
    },
};

$(function () {
    // Inicializacion de variables
    let english = $('#en');
    let portuguese = $('#pt');
    let spanish = $('#es');

    // Ingles
    english.on('click', function (e) {
        e.preventDefault();
        let translator = $('body').translate({lang: "en", t: dict});
        localStorage.setItem('lang', 'en');

        // Obtener traducciones
        let name = translator.get("Nombre:");
        let apellido = translator.get("Apellido:");
        let empresa = translator.get("Empresa:");
        let pais = translator.get("País:");
        let email = translator.get("Email:");
        let ayuda = translator.get("¿Cómo podemos ayudarlo?:");

        // Reemplazar en el form
        $('#inputName').attr('placeholder', name);
        $('#inputLastname').attr('placeholder', apellido);
        $('#inputCompany').attr('placeholder', empresa);
        $('#inputCountry').attr('placeholder', pais);
        $('#inputEmail').attr('placeholder', email);
        $('#validationTextarea').attr('placeholder', ayuda);
    });

    // Spanish
    spanish.on('click', function (e) {
        e.preventDefault();
        let translator = $('body').translate({lang: "es", t: dict});
        localStorage.setItem('lang', 'es');

        // Obtener traducciones
        let name = translator.get("Nombre:");
        let apellido = translator.get("Apellido:");
        let empresa = translator.get("Empresa:");
        let pais = translator.get("País:");
        let email = translator.get("Email:");
        let ayuda = translator.get("¿Cómo podemos ayudarlo?:");

        // Reemplazar en el form
        $('#inputName').attr('placeholder', name);
        $('#inputLastname').attr('placeholder', apellido);
        $('#inputCompany').attr('placeholder', empresa);
        $('#inputCountry').attr('placeholder', pais);
        $('#inputEmail').attr('placeholder', email);
        $('#validationTextarea').attr('placeholder', ayuda);
    });

    // Portugues
    portuguese.on('click', function (e) {
        e.preventDefault();
        let translator = $('body').translate({lang: "pt", t: dict});
        localStorage.setItem('lang', 'pt');

        // Obtener traducciones
        let name = translator.get("Nombre:");
        let apellido = translator.get("Apellido:");
        let empresa = translator.get("Empresa:");
        let pais = translator.get("País:");
        let email = translator.get("Email:");
        let ayuda = translator.get("¿Cómo podemos ayudarlo?:");

        // Reemplazar en el form
        $('#inputName').attr('placeholder', name);
        $('#inputLastname').attr('placeholder', apellido);
        $('#inputCompany').attr('placeholder', empresa);
        $('#inputCountry').attr('placeholder', pais);
        $('#inputEmail').attr('placeholder', email);
        $('#validationTextarea').attr('placeholder', ayuda);
    });
    // Set defualt language
    const lang = localStorage.getItem('lang');
    if (lang === "en"){
        $( "#en" ).trigger( "click" );
    }
    if (lang === "es"){
        $( "#es" ).trigger( "click" );
    }
    if (lang === "pt"){
        $( "#pt" ).trigger( "click" );
    }
    if ( lang == null){
        $( "#en" ).trigger( "click" );
    }

});
