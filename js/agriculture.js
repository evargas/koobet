
// Diccionario
const dict = {
    // Menu
    "Quienes somos": {
        pt: "Quem somos",
        en: "About Us",
        es: "Quienes somos"
    },
    "SOLUTIONS": {
        pt: "SOLUÇÕES",
        en: "SOLUTIONS",
        es: "SOLUCIONES"
    },
    "Industrias": {
        pt: "Indústrias",
        en: "Industry",
        es: "Industrias"
    },
    "Sectores": {
        pt: "Sectores",
        en: "Sectors",
        es: "Sectores"
    },
    "Cobertura geográfica": {
        pt: "Cobertura geográfica",
        en: "Locations",
        es: "Cobertura geográfica"
    },
    "Contacto": {
        pt: "Fale conosco",
        en: "Contact Us",
        es: "Contacto"
    },
    // Sub menu
    "Agricultura / Salud Animal": {
        pt: "Agrucultura / Saúde Animal",
        en: "Agruculture / Animal Health",
        es: "Agricultura / Salud Animal"
    },
    "B2B / Industrial": {
        pt: "B2B / Industrial",
        en: "B2B / Industry",
        es: "B2B / Industrial"
    },
    "Salud / Farma": {
        pt: "Saúde / Farma",
        en: "Healthcare / Pharma",
        es: "Salud / Farma"
    },
    "Consumo": {
        pt: "Consumo",
        en: "Consumer",
        es: "Consumo"
    },
    "Desarrollo de negocios": {
        pt: "Desenvolvimiento de Negócios",
        en: "Business Development",
        es: "Desarrollo de negocios"
    },
    // Seccion 1
    "Market Research & Business Development": {
        pt: "Market Research & Business Development",
        en: "Market Research & Business Development",
        es: "Market Research & Business Development"
    },
    "COMPRENDIENDO Y CONECTANDO": {
        pt: "COMPRENDENDO E CONECTANDO",
        en: "UNDERSTANDING AND CONNECTING",
        es: "COMPRENDIENDO Y CONECTANDO"
    },
    " CON SU AUDIENCIA OBJETIVO": {
        pt: "COM SEU PÚBLICO ALVO",
        en: "WITH YOUR TARGET AUDIENCE",
        es: " CON SU AUDIENCIA OBJETIVO"
    },
    // Quienes somos
    "QUIÉNES <br> SOMOS": {
        pt: "QUEM SOMOS",
        en: "ABOUT US",
        es: "QUIÉNES SOMOS"
    },
    "Con más de <strong>10 años</strong> de experiencia en investigación de mercado agrícola y salud animal, nuestra fortaleza se basa en una combinación única de conocimiento especializado del sector y experiencia en investigación en el país.": {
        pt: "Com mais de 10 anos de experiência em pesquisa de mercado agrícola e de saúde animal, nossa força se baseia numa combinação única de conhecimento especializado do setor e experiência em pesquisas no país.",
        en: "With more than 10 years of experience in agricultural and animal health research, our strength is based on a unique combination of industry expertise and research experience in the countries where we operate.",
        es: "Con más de <strong>10 años</strong> de experiencia en investigación de mercado agrícola y salud animal, nuestra fortaleza se basa en una combinación única de conocimiento especializado del sector y experiencia en investigación en el país."
    },
    "Cubrimos distintos cultivos como soja, maíz, trigo, girasol, café, caña de azúcar, frutas, vegetales y especies como bovino de carne, bovino de leche, avícola, porcícola, acuicultura, animales de compañía y maquinaria agrícola.": {
        pt: "Cobrimos diferentes cultivos como soja, milho, trigo, girassol, café, cana de açúcar, frutas, vegetais e espécies como bovino de carne, bovino de leite, avícola, suínos, aquicultura, animais de estimação e maquinário agrícola.",
        en: "We cover different crops, such as soybeans, corn, wheat, sunflower, coffee, sugarcane, fruits and vegetables, and animal species such as beef cattle, dairy cattle, poultry, swine, aquaculture, companion animals and Ag machinery.",
        es: "Cubrimos distintos cultivos como soja, maíz, trigo, girasol, café, caña de azúcar, frutas, vegetales y especies como bovino de carne, bovino de leche, avícola, porcícola, acuicultura, animales de compañía y maquinaria agrícola."
    },
    "De acuerdo a las necesidades de nuestros clientes conducimos estudios de satisfacción, test de conceptos y productos, estudios de optimización de precios, evaluación y oportunidades de mercado, segmentación y posicionamiento, evaluación de imagen y valor de marca, factores clave de compra, entre otros.": {
        pt: "De acordo com as necessidades dos nossos clientes conduzimos estudos de satisfação, teste de conceitos e produtos, estudos de otimização de preço, avaliação e oportunidades de mercado, segmentação e posicionamento, avaliação de imagem e valor de marca, pesquisa de fatores de compra, entre outros.",
        en: "In accordance with our clients’ needs, we can conduct customer satisfaction / loyalty studies, concept & product testing, pricing optimization, market and opportunities assessment, market segmentation, brand equity and image assessment, brand positioning, usage & attitude studies, among others.",
        es: "De acuerdo a las necesidades de nuestros clientes conducimos estudios de satisfacción, test de conceptos y productos, estudios de optimización de precios, evaluación y oportunidades de mercado, segmentación y posicionamiento, evaluación de imagen y valor de marca, factores clave de compra, entre otros."
    },
    "Dependiendo del tipo de estudio nuestras fuentes de información pueden ser productores, veterinarios nutricionistas, asesores, expertos, retailers, distribuidores, consumidores y fabricantes.": {
        pt: "Dependendo do tipo de estudo, nossas fontes de informação podem ser produtores, veterinários, nutricionistas, assessores, especialistas, retailers, distribuidores, consumidores e fabricantes.",
        en: "Depending on the type of study, our sources of information may be farmers, veterinarians, nutritionists, advisors, experts, retailers, distributors, consumers and manufacturers.",
        es: "Dependiendo del tipo de estudio nuestras fuentes de información pueden ser productores, veterinarios nutricionistas, asesores, expertos, retailers, distribuidores, consumidores y fabricantes."
    },

    // Servicios
    "Sectores de <br>especialización": {
        pt: "Setores de <br>especialização",
        en: "SEctors OF <br>specialization",
        es: "Sectores de <br>especialización"
    },
    "Algunos cultivos y especies cubiertas, y tipo de respondentes que entrevistamos son:": {
        pt: "Alguns cultivos e espécies que abrangemos e tipo de entrevistados são",
        en: "Some crops and species that we cover, and the types of respondents are",
        es: "Algunos cultivos y especies cubiertas, y tipo de respondentes que entrevistamos son"
    },
    "AGRICULTURA": {
        pt: "Agricultura",
        en: "AGRICULTURE",
        es: "AGRICULTURA"
    },
    "Cultivos": {
        pt: "Cultivos",
        en: "Crops:",
        es: "Cultivos"
    },
    "Soja, Maíz, Trigo, Girasol, Café, Caña de Azúcar, Algodón, Frutas y Vegetales.": {
        pt: "Soja, Milho, Trigo, Girassol, Café, Cana de Açúcar, Algodão, Frutas e Vegetais.",
        en: "Soybean, Corn, Wheat, Sunflower, Coffee, Sugarcane, Cotton, Fruits & Vegetables.",
        es: "Soja, Maíz, Trigo, Girasol, Café, Caña de Azúcar, Algodón, Frutas y Vegetales."
    },
    "types_of_respondents": {
        pt: "Tipos de respondentes:",
        en: "Types of respondents:",
        es: "Tipos de respondentes:"
    },
    "Productores, Asesores, Retailers, Distribuidores, Fabricantes.": {
        pt: "Produtores, Assessores, Retailers, Distribuidores e Fabricantes.",
        en: "Producers, Advisors, Retailers, Distributors, Manufacturers.",
        es: "Productores, Asesores, Retailers, Distribuidores, Fabricantes."
    },
    "SALUD ANIMAL": {
        pt: "Saúde ANIMAL",
        en: "ANIMAL HEALTH",
        es: "SALUD ANIMAL"
    },
    "Especies": {
        pt: "Espécies",
        en: "Species",
        es: "Especies"
    },
    "Bovino, Avícola, Porcícola, Equino, Acuicultura, Animales de compañía.": {
        pt: "Bovino, Avícola, Suíno, Equino, Aquicultura e Animais de estimação",
        en: "Dairy Cattle, Beef Cattle, Poultry, Swine, Equine, Aquiculture and Companion Animal",
        es: "Bovino, Avícola, Porcícola, Equino, Acuicultura, Animales de compañía."
    },
    "Productores, Veterinarios, Nutricionistas, Retailers, Dueños de mascotas.": {
        pt: "Produtores, Veterinários, Nutricionistas, Retailers e Donos de animais de estimação.",
        en: "Producers, Veterinaries, Nutritionists, Retailers and Companion Animal Owners",
        es: "Productores, Veterinarios, Nutricionistas, Retailers, Dueños de mascotas."
    },
    "MAQUINARIA AGRÍCOLA": {
        pt: "MAQUINARIA AGRÍCOLA",
        en: "AGRICULTURAL MACHINERY",
        es: "MAQUINARIA AGRÍCOLA"
    },
    "Equipamento": {
        pt: "Equipamento",
        en: "Equipment",
        es: "Equipamento"
    },
    "Sembradoras, Cosechadoras, Pulverizadoras, Tractores, Agricultura de precisión, Equipos de Forraje y Cargadores frontales.": {
        pt: "Plantadeiras, Colheitadeiras, Pulverizadores, Tratores, Agricultura de precisão, Equipamento Forrageiro e Carregadores Frontais",
        en: "Planting Equipment, Harvesting Equipment, Sprayers, Tractors, Precision Agricultural Technology, Forage Equipment and Front Loaders.",
        es: "Sembradoras, Cosechadoras, Pulverizadoras, Tractores, Agricultura de precisión, Equipos de Forraje y Cargadores frontales."
    },
    "Productores agrícolas y Pecuaristas.": {
        pt: "Produtores agrícolas e Pecuaristas:",
        en: "Agricultural and Livestock Farmers.",
        es: "Productores agrícolas y Pecuaristas."
    },
    // Investigacion ad hoc
    "INVESTIGACIÓN AD HOC PARA SUS NECESIDADES": {
        pt: "Pesquisa ad hoc para suas necessidades",
        en: "Ad hoc research for your needs",
        es: "INVESTIGACIÓN AD HOC PARA SUS NECESIDADES"
    },
    "Estudios de satisfacción y lealtad del cliente": {
        pt: "Pesquisa de satisfação / lealdade dos clientes",
        en: "Customer Satisfaction and Loyalty",
        es: "Estudios de satisfacción y lealtad del cliente"
    },
    "Test de conceptos y productos": {
        pt: "Testes de conceitos e produtos",
        en: "Concept Testing",
        es: "Test de conceptos y productos"
    },
    "Valor y optimización de precios": {
        pt: "Valor e otimização de preços",
        en: "Value & Pricing Optimization",
        es: "Valor y optimización de precios"
    },
    "Evaluación y oportunidades de mercado": {
        pt: "Avaliação e oportunidades de mercado",
        en: "Market & Opportunities Assessment",
        es: "Evaluación y oportunidades de mercado"
    },
    "Segmentación de mercado": {
        pt: "Segmentação de mercado",
        en: "Market Segmentation",
        es: "Segmentación de mercado"
    },
    "Evaluación de imagen y valor de marca": {
        pt: "Avaliação de imagem e valor de marca",
        en: "Brand Equity & Image",
        es: "Evaluación de imagen y valor de marca"
    },
    "Posicionamiento de marca": {
        pt: "Posicionamento de marca",
        en: "Brand Positioning",
        es: "Posicionamiento de marca"
    },
    "Conocimiento y percepción de marca": {
        pt: "Conhecimento e percepção da marca",
        en: "Brand Awareness and Perception",
        es: "Conocimiento y percepción de marca"
    },
    "Estudios de usos y actitudes": {
        pt: "Estudos de usos e atitudes",
        en: "Usage & Attitude Studies",
        es: "Estudios de usos y actitudes"
    },
    "Investigación de factores de compra": {
        pt: "Pesquisa de fatores de compra",
        en: "Buying Factor Research",
        es: "Investigación de factores de compra"
    },
    // Metodologias
    "TIPOS DE ESTUDIO Y METODOLOGÍAS": {
        pt: "Tipos de estudos e metodologias ",
        en: "Types of studies and methodologies",
        es: "TIPOS DE ESTUDIO Y METODOLOGÍAS"
    },
    "Según el tipo de estudio podemos usar alguna o una combinación de las siguientes metodologías:": {
        pt: "Segundo o tipo de estudo podemos usar alguma ou uma combinação das seguintes metodologias:",
        en: "Depending on the type of study, we can use some or a combination of the following methodologies:",
        es: "Según el tipo de estudio podemos usar alguna o una combinación de las siguientes metodologías:"
    },
    "Estudios Cualitativos": {
        pt: "Estudos qualitativos",
        en: "Qualitative research",
        es: "Estudios Cualitativos"
    },
    "Entrevistas en profundidad personales ": {
        pt: "Entrevistas em profundidade pessoais",
        en: "Face to face in-depth interviews ",
        es: "Entrevistas en profundidad personales "
    },
    "Entrevistas en profundidad telefónicas": {
        pt: "Entrevistas em profundidade telefônicas",
        en: "Telephone in-depth interviews",
        es: "Entrevistas en profundidad telefónicas"
    },
    "Estudios Cuantitativos": {
        pt: "Estudios Cuantitativos",
        en: "Quantitative research",
        es: "Estudios Cuantitativos"
    },
    "Entrevistas personales (PAPI, CAPI)": {
        pt: "Entrevistas pessoais (PAPI, CAPI)",
        en: "Face to face interviews (PAPI, CAPI)",
        es: "Entrevistas personales (PAPI, CAPI)"
    },
    "Entrevistas telefónicas (CATI)": {
        pt: "Entrevistas telefônicas (CATI)",
        en: "Telephone interviews (CATI)",
        es: "Entrevistas telefónicas (CATI)"
    },
    "Encuestas Online": {
        pt: "Entrevistas online",
        en: "Online surveys",
        es: "Encuestas Online"
    },
    // Seccion 2
    "INDUSTRIAS ATENDIDAS": {
        pt: "INDÚSTRIAS ATENDIDAS",
        en: "Industries we served",
        es: "INDUSTRIAS ATENDIDAS"
    },
    "Alimentos y Bebidas": {
        pt: "Alimentos e Bebidas",
        en: "Food and Beverage",
        es: "Alimentos y Bebidas"
    },
    "Aeronáutica": {
        pt: "Aeronáutica",
        en: "Aerospace",
        es: "Aeronáutica"
    },
    "Agropecuario": {
        pt: "Agropecuário",
        en: "Agriculture ",
        es: "Agropecuario"
    },
    "Automotriz": {
        pt: "Automotivo",
        en: "Automotive",
        es: "Automotriz"
    },
    "Biotecnología": {
        pt: "Biotecnologia ",
        en: "Biotechnology",
        es: "Biotecnología"
    },
    "Química": {
        pt: "Quimica ",
        en: "Chemical ",
        es: "Química"
    },
    "Construcción": {
        pt: "Construção ",
        en: "Construction ",
        es: "Construcción"
    },
    "Cosmética y Belleza": {
        pt: "Cosmética e Beleza",
        en: "Cosmetics and Beauty  ",
        es: "Cosmética y Belleza"
    },
    "Educación": {
        pt: "Educação",
        en: "Education ",
        es: "Educación"
    },
    "Eléctrico": {
        pt: "Elétrico",
        en: "Electric",
        es: "Eléctrico"
    },
    "Electrónica": {
        pt: "Eletrônica",
        en: "Electronics",
        es: "Electrónica"
    },
    "Energía": {
        pt: "Energia",
        en: "Energy ",
        es: "Energía"
    },
    "Entretenimiento": {
        pt: "Entretenimento",
        en: "Entertainment ",
        es: "Entretenimiento"
    },
    "Farmacéutico": {
        pt: "Farmacêutico",
        en: "",
        es: "Farmacéutico"
    },
    "Financiero": {
        pt: "Financeiro",
        en: "Finances",
        es: "Financiero"
    },
    "Franquicias": {
        pt: "Franquias ",
        en: "Franchises",
        es: "Franquicias"
    },
    "Gobierno": {
        pt: "Governo ",
        en: "Goverment",
        es: "Gobierno"
    },
    "Herramientas": {
        pt: "Hotelaria ",
        en: "Haardware",
        es: "Herramientas"
    },
    "Hotelería": {
        pt: "Ferramentas",
        en: "Hotel",
        es: "Hotelería"
    },
    "Iluminación": {
        pt: "Iluminação ",
        en: "Lightning",
        es: "Iluminación"
    },
    "Ingredientes": {
        pt: "Ingredientes",
        en: "Ingredients",
        es: "Ingredientes"
    },
    "Medio Ambiente": {
        pt: "Meio Ambiente",
        en: "Enviromental",
        es: "Medio Ambiente"
    },
    "Minería y Metales": {
        pt: "Mineração e Metais",
        en: "Mining and Metals",
        es: "Minería y Metales"
    },
    "Packaging": {
        pt: "Embalagem ",
        en: "Packaging",
        es: "Packaging"
    },
    "Petróleo y Gas": {
        pt: "Petróleo e Gás",
        en: "Oil and Gas",
        es: "Petróleo y Gas"
    },
    "Retail": {
        pt: "Retail",
        en: "Retail",
        es: "Retail"
    },
    "Salud": {
        pt: "Saúde ",
        en: "Healthcare",
        es: "Salud"
    },
    "Seguridad": {
        pt: "Segurança",
        en: "Safety and Security",
        es: "Seguridad"
    },
    "Servicios Públicos": {
        pt: "Serviços Públicos",
        en: "Public Services",
        es: "Servicios Públicos"
    },
    "Tecnología": {
        pt: "Tecnologia ",
        en: "Technology",
        es: "Tecnología"
    },
    "Textil": {
        pt: "Têxtil ",
        en: "Textiles",
        es: "Textil"
    },
    "Transporte": {
        pt: "Transporte ",
        en: "Transportation",
        es: "Transporte"
    },
    "Servicios financieros": {
        pt: "Serviços financeiros",
        en: "Financial Services",
        es: "Servicios financieros"
    },
    "Servicios profesionales": {
        pt: "Servicios profesionales",
        en: "Professional Services",
        es: "Servicios profesionales"
    },
    "Telecomunicaciones": {
        pt: "Telecomunicações",
        en: "Telecommunication",
        es: "Telecomunicaciones"
    },
    "Transporte y Logística": {
        pt: "Transporte e Logística",
        en: "Transportation and Logistic",
        es: "Transporte y Logística"
    },
    // Seccion 3
    "PAÍSES<br>DONDE OPERAMOS": {
        pt: "PAÍSES<br>ONDE OPERAMOS",
        en: "Countries <br> where we operate",
        es: "PAÍSES<br>DONDE OPERAMOS"
    },
    "Argentina": {
        pt: "Argentina",
        en: "Argentina",
        es: "Argentina"
    },
    "Bolivia": {
        pt: "Bolivia",
        en: "Bolivia",
        es: "Bolivia"
    },
    "Brasil": {
        pt: "Brasil",
        en: "Brazil",
        es: "Brasil"
    },
    "Chile": {
        pt: "Chile",
        en: "Chile",
        es: "Chile"
    },
    "Colombia": {
        pt: "Colômbia",
        en: "Colombia",
        es: "Colombia"
    },
    "Costa Rica": {
        pt: "Costa Rica",
        en: "",
        es: "Costa Rica"
    },
    "Ecuador": {
        pt: "Ecuador",
        en: "Ecuador",
        es: "Ecuador"
    },
    "Guatemala": {
        pt: "Guatemala",
        en: "Guatemala",
        es: "Guatemala"
    },
    "Honduras": {
        pt: "Honduras",
        en: "Honduras",
        es: "Honduras"
    },
    "México": {
        pt: "México",
        en: "Mexico",
        es: "México"
    },
    "Nicaragua": {
        pt: "Nicaragua",
        en: "Nicaragua",
        es: "Nicaragua"
    },
    "Panamá": {
        pt: "Panamá",
        en: "Panama",
        es: "Panamá"
    },
    "Paraguay": {
        pt: "Paraguai",
        en: "Paraguay",
        es: "Paraguay"
    },
    "Perú": {
        pt: "Perú",
        en: "Peru",
        es: "Perú"
    },
    "República Dominicana": {
        pt: "Rep. Dominicana",
        en: "Dominican Republic",
        es: "República Dominicana"
    },
    "Uruguay": {
        pt: "Uruguai",
        en: "Uruguay",
        es: "Uruguay"
    },
    // Formulario de contacto
    "CONTÁCTENOS": {
        pt: "Fale conosco",
        en: "Contact Us",
        es: "CONTÁCTENOS"
    },
    "Ayúdenos a comprender sus necesidades y cómo podríamos servirle.": {
        pt: "Nos ajude a compreender suas necesidades e como podemos serví-lo.",
        en: "Please help us understand your needs and how we can best help you",
        es: "Ayúdenos a comprender sus necesidades y cómo podríamos servirle."
    },
    "Nombre:": {
        pt: "Nome:",
        en: "First name:",
        es: "Nombre:"
    },
    "Apellido:": {
        pt: "Sobrenome:",
        en: "Last name:",
        es: "Apellido:"
    },
    "Empresa:": {
        pt: "Empresa:",
        en: "Company:",
        es: "Empresa:"
    },
    "País:": {
        pt: "País:",
        en: "Country:",
        es: "País:"
    },
    "Email:": {
        pt: "Email:",
        en: "Email:",
        es: "Email:"
    },
    "¿Cómo podemos ayudarlo?:": {
        pt: "Como podemos te ajudar?:",
        en: "How can we help you?:",
        es: "¿Cómo podemos ayudarlo?:"
    },
    "Enviar": {
        pt: "Enviar",
        en: "Submit",
        es: "Enviar"
    },
};

$(function () {
    // Inicializacion de variables
    let english = $('#en');
    let portuguese = $('#pt');
    let spanish = $('#es');

    // Ingles
    english.on('click', function (e) {
        e.preventDefault();
        let translator = $('body').translate({lang: "en", t: dict});
        localStorage.setItem('lang', 'en');

        // Obtener traducciones
        let name = translator.get("Nombre:");
        let apellido = translator.get("Apellido:");
        let empresa = translator.get("Empresa:");
        let pais = translator.get("País:");
        let email = translator.get("Email:");
        let ayuda = translator.get("¿Cómo podemos ayudarlo?:");

        // Reemplazar en el form
        $('#inputName').attr('placeholder', name);
        $('#inputLastname').attr('placeholder', apellido);
        $('#inputCompany').attr('placeholder', empresa);
        $('#inputCountry').attr('placeholder', pais);
        $('#inputEmail').attr('placeholder', email);
        $('#validationTextarea').attr('placeholder', ayuda);
    });

    // Spanish
    spanish.on('click', function (e) {
        e.preventDefault();
        let translator = $('body').translate({lang: "es", t: dict});
        localStorage.setItem('lang', 'es');

        // Obtener traducciones
        let name = translator.get("Nombre:");
        let apellido = translator.get("Apellido:");
        let empresa = translator.get("Empresa:");
        let pais = translator.get("País:");
        let email = translator.get("Email:");
        let ayuda = translator.get("¿Cómo podemos ayudarlo?:");

        // Reemplazar en el form
        $('#inputName').attr('placeholder', name);
        $('#inputLastname').attr('placeholder', apellido);
        $('#inputCompany').attr('placeholder', empresa);
        $('#inputCountry').attr('placeholder', pais);
        $('#inputEmail').attr('placeholder', email);
        $('#validationTextarea').attr('placeholder', ayuda);
    });

    // Portugues
    portuguese.on('click', function (e) {
        e.preventDefault();
        let translator = $('body').translate({lang: "pt", t: dict});
        localStorage.setItem('lang', 'pt');

        // Obtener traducciones
        let name = translator.get("Nombre:");
        let apellido = translator.get("Apellido:");
        let empresa = translator.get("Empresa:");
        let pais = translator.get("País:");
        let email = translator.get("Email:");
        let ayuda = translator.get("¿Cómo podemos ayudarlo?:");

        // Reemplazar en el form
        $('#inputName').attr('placeholder', name);
        $('#inputLastname').attr('placeholder', apellido);
        $('#inputCompany').attr('placeholder', empresa);
        $('#inputCountry').attr('placeholder', pais);
        $('#inputEmail').attr('placeholder', email);
        $('#validationTextarea').attr('placeholder', ayuda);
    });
    // Set defualt language
    const lang = localStorage.getItem('lang');
    if (lang === "en"){
        $( "#en" ).trigger( "click" );
    }
    if (lang === "es"){
        $( "#es" ).trigger( "click" );
    }
    if (lang === "pt"){
        $( "#pt" ).trigger( "click" );
    }
    if ( lang == null){
        $( "#en" ).trigger( "click" );
    }

});

