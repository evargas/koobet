
// Diccionario
const dict = {
    // Menu
    "Quienes somos": {
        pt: "Quem somos",
        en: "About Us",
        es: "Quienes somos"
    },
    "Servicios": {
        pt: "Servicios",
        en: "Services",
        es: "Servicios"
    },
    "Industrias": {
        pt: "Indústrias",
        en: "Industries",
        es: "Industrias"
    },
    "Sectores": {
        pt: "Sectores",
        en: "Sectors",
        es: "Sectores"
    },
    "Cobertura geográfica": {
        pt: "Cobertura geográfica",
        en: "Locations",
        es: "Cobertura geográfica"
    },
    "Contacto": {
        pt: "Fale conosco",
        en: "Contact Us",
        es: "Contacto"
    },
    // Sub menu
    "Agricultura / Salud Animal": {
        pt: "Agrucultura e Saúde Animal",
        en: "Agruculture and Animal Health",
        es: "Agricultura / Salud Animal"
    },
    "B2B / Industrial": {
        pt: "B2B / Industrial",
        en: "B2B / Industrial",
        es: "B2B / Industrial"
    },
    "Salud / Farma": {
        pt: "Saúde / Farma",
        en: "Healthcare / Pharma",
        es: "Salud / Farma"
    },
    "Consumo": {
        pt: "Consumo",
        en: "Consumer",
        es: "Consumo"
    },
    "Desarrollo de negocios": {
        pt: "Desenvolvimiento de Negócios",
        en: "Business Development",
        es: "Desarrollo de negocios"
    },
    // Seccion 1
    "Market Research & Business Development": {
        pt: "Market Research & Business Development",
        en: "Market Research & Business Development",
        es: "Market Research & Business Development"
    },
    "COMPRENDIENDO Y CONECTANDO": {
        pt: "COMPRENDENDO E CONECTANDO",
        en: "UNDERSTANDING AND CONNECTING",
        es: "COMPRENDIENDO Y CONECTANDO"
    },
    " CON SU AUDIENCIA OBJETIVO": {
        pt: "COM SEU PÚBLICO ALVO",
        en: "WITH YOUR TARGET AUDIENCE",
        es: " CON SU AUDIENCIA OBJETIVO"
    },
    // Quienes somos
    "QUIÉNES <br> SOMOS": {
        pt: "QUEM <br> SOMOS",
        en: "ABOUT US",
        es: "QUIÉNES <br> SOMOS"
    },
    "Hace más de <strong>10 años</strong> ayudamos a empresas a competir en Latinoamérica proporcionando información clave sobre clientes, competidores, y entorno del mercado.": {
        pt: "Há mais de 10 anos ajudamos a empresas a competir na América Latina, proporcionando informações essenciais sobre clientes, concorrentes e sobre o panorama do mercado.",
        en: "For more than 10 years, we have been helping companies to compete in Latin America by providing essential information on customers, competitors and the market landscape.",
        es: "Hace más de <strong>10 años</strong> ayudamos a empresas a competir en Latinoamérica proporcionando información clave sobre clientes, competidores, y entorno del mercado."
    },
    "Cubrimos una amplia gama de industrias como Química, Construcción, Energía, Salud, Automotriz, Servicios financieros, Alimentos y Bebidas, IT, Telecomunicaciones, Retail, Transporte y Logística, Servicios profesionales.": {
        pt: "Cobrimos uma ampla gama de indústrias como Química, Construção, Energia, Saúde, Automotiva, Serviços financeiros, Alimentos e Bebida, TI, Telecomunicações, Retail, Transporte e Logística, Serviços Profissionais.",
        en: "We cover a wide range of industries such as Chemical, Construction, Energy, Healthcare, Automotive, Financial Services, Food & Beverage, IT, Telecommunications, Retail, Transportation & Logistics and Professional Services.",
        es: "Cubrimos una amplia gama de industrias como Química, Construcción, Energía, Salud, Automotriz, Servicios financieros, Alimentos y Bebidas, IT, Telecomunicaciones, Retail, Transporte y Logística, Servicios profesionales."
    },
    "Operamos en Argentina, Bolivia, Brasil, Chile, Colombia, Costa Rica, Cuba, Ecuador, El Salvador, Guatemala, Honduras, México, Nicaragua, Panamá, Paraguay, Perú, República Dominicana y Uruguay.": {
        pt: "Operamos na Argentina, Brasil, Bolívia, Chile, Colômbia, Costa Rica, Cuba, Equador, El Salvador, Guatemala, Honduras, México, Nicarágua, Panamá, Paraguai, Peru, Rep. Dominicana e Uruguai.",
        en: "We operate in Argentina, Brazil, Bolivia, Chile, Colombia, Costa Rica, Cuba, Dominican Rep. Ecuador, El Salvador, Guatemala, Honduras, Mexico, Nicaragua, Panama, Paraguay, Peru and Uruguay.",
        es: "Operamos en Argentina, Bolivia, Brasil, Chile, Colombia, Costa Rica, Cuba, Ecuador, El Salvador, Guatemala, Honduras, México, Nicaragua, Panamá, Paraguay, Perú, República Dominicana y Uruguay."
    },
    "De acuerdo a las necesidades de nuestros clientes podemos conducir estudios de satisfacción, test de conceptos, estudio de precios, conocimiento e imagen de marca, segmentación y posicionamiento, tamaño del mercado, estudio de competidores, entre otros.": {
        pt: "De acordo com as necessidades de nossos clientes, podemos conduzir estudos de satisfação, teste de conceito, estudo de preços, conhecimentos e imagem da marca, segmentação e posicionamentos, tamanho de mercado, estudo de concorrentes, entre outros.",
        en: "In accordance with our clients’ needs, we can conduct customer satisfaction research, concept test, pricing studies, brand equity and image, segmentation and positioning, market sizing, and competitor studies, among others.",
        es: "De acuerdo a las necesidades de nuestros clientes podemos conducir estudios de satisfacción, test de conceptos, estudio de precios, conocimiento e imagen de marca, segmentación y posicionamiento, tamaño del mercado, estudio de competidores, entre otros."
    },
    "Nuestras fuentes de información son fabricantes, importadores, distribuidores, y expertos en la industria.": {
        pt: "Nossas fontes de informação são fabricantes, importadores, distribuidores e especialistas na indústria.",
        en: "Our sources of information are manufacturers, importers, distributors and industry experts.",
        es: "Nuestras fuentes de información son fabricantes, importadores, distribuidores, y expertos en la industria."
    },

    // Servicios
    "NUESTROS <br> SERVICIOS": {
        pt: "Nossos <br> Serviços",
        en: "OUR SERVICES",
        es: "NUESTROS <br> SERVICIOS"
    },
    // subtitulo
    "Mediante abordajes cualitativos y cuantitativos conducimos una variedad de estudios, incluyendo:": {
        pt: "Mediante a abordagens qualitativas e quantitativas, conduzimos uma variedade de estudos, incluindo:",
        en: "Through qualitative and quantitative approaches, we conduct a variety of studies, including:",
        es: "Mediante abordajes cualitativos y cuantitativos conducimos una variedad de estudios, incluyendo:"
    },
    "Tamaño y crecimiento de mercado": {
        pt: "Tamanho e taxa de crescimento de mercado",
        en: "Market Sizing & Growth",
        es: "Tamaño y crecimiento de mercado"
    },
    "Análisis de la cadena de valor": {
        pt: "Análise da cadeia de valor",
        en: "Value Chain Analysis",
        es: "Análisis de la cadena de valor"
    },
    "Segmentación y oportunidades de mercado": {
        pt: "Segmentação  e oportunidades de mercado",
        en: "Market Segmentation & Opportunities",
        es: "Segmentación y oportunidades de mercado"
    },
    "Entorno Competitivo": {
        pt: "Panorama competitivo",
        en: "Competitive Landscape",
        es: "Entorno Competitivo"
    },
    "Imagen y valor de marca": {
        pt: "Necessidades do cliente",
        en: "Brand Equity and Image",
        es: "Imagen y valor de marca"
    },
    "Test de concepto": {
        pt: "Teste de conceito",
        en: "Concept Testing",
        es: "Test de concepto"
    },
    "Precio y elasticidad del precio": {
        pt: "Preço e elasticidade do preço",
        en: "Pricing and Price Elasticity",
        es: "Precio y elasticidad del precio"
    },
    "Tracking de conocimiento, prueba y uso": {
        pt: "Acompanhamento do conhecimento, teste e uso",
        en: "Usage and Attitude Tracking",
        es: "Tracking de conocimiento, prueba y uso"
    },
    "Perfil y evaluación de competidores": {
        pt: "Perfil e avaliação de concorrentes",
        en: "Competitor Profiling / Assessment",
        es: "Perfil y evaluación de competidores"
    },
    "Posicionamiento y benchmarking": {
        pt: "Posicionamento e benchmarking",
        en: "Positioning and benchmarking",
        es: "Posicionamiento y benchmarking"
    },
    "Percepción del mercado": {
        pt: "Percepção do mercado",
        en: "Market Perception",
        es: "Percepción del mercado"
    },
    "Necesidades del cliente": {
        pt: "Imagem e valor de marca",
        en: "Customer Needs",
        es: "Necesidades del cliente"
    },
    "Satisfacción y Lealtad del cliente": {
        pt: "Lealdade do cliente",
        en: "Customer Satisfaction & Loyalty",
        es: "Satisfacción y Lealtad del cliente"
    },
    "Voz del cliente": {
        pt: "Voz do cliente",
        en: "Voice of the Customer",
        es: "Voz del cliente"
    },
    // subtitulo
    "METODOLOGÍAS EMPLEADAS": {
        pt: "Metodologias utilizadas",
        en: "Methodologies",
        es: "METODOLOGÍAS EMPLEADAS"
    },
    "Entrevistas personales": {
        pt: "Entrevistas pessoais",
        en: "Face-to-face Interviews",
        es: "Entrevistas personales"
    },
    "Entrevistas telefónicas": {
        pt: "Entrevistas telefônicas",
        en: "Telephone Interviews",
        es: "Entrevistas telefónicas"
    },
    "Encuestas Online": {
        pt: "Entrevistas online",
        en: "Online interviews",
        es: "Encuestas Online"
    },
    // Seccion 2
    "INDUSTRIAS ATENDIDAS": {
        pt: "INDÚSTRIAS ATENDIDAS",
        en: "Industries we served",
        es: "INDUSTRIAS ATENDIDAS"
    },
    "Alimentos y Bebidas": {
        pt: "Alimentos e Bebidas",
        en: "Food and Beverage",
        es: "Alimentos y Bebidas"
    },
    "Aeronáutica": {
        pt: "Aeronáutica",
        en: "Aerospace",
        es: "Aeronáutica"
    },
    "Agropecuario": {
        pt: "Agropecuário",
        en: "Agriculture ",
        es: "Agropecuario"
    },
    "Automotriz": {
        pt: "Automotivo",
        en: "Automotive",
        es: "Automotriz"
    },
    "Biotecnología": {
        pt: "Biotecnologia ",
        en: "Biotechnology",
        es: "Biotecnología"
    },
    "Química": {
        pt: "Quimica ",
        en: "Chemical ",
        es: "Química"
    },
    "Construcción": {
        pt: "Construção ",
        en: "Construction ",
        es: "Construcción"
    },
    "Cosmética y Belleza": {
        pt: "Cosmética e Beleza",
        en: "Cosmetics and Beauty  ",
        es: "Cosmética y Belleza"
    },
    "Educación": {
        pt: "Educação",
        en: "Education ",
        es: "Educación"
    },
    "Eléctrico": {
        pt: "Elétrico",
        en: "Electric",
        es: "Eléctrico"
    },
    "Electrónica": {
        pt: "Eletrônica",
        en: "Electronics",
        es: "Electrónica"
    },
    "Energía": {
        pt: "Energia",
        en: "Energy ",
        es: "Energía"
    },
    "Entretenimiento": {
        pt: "Entretenimento",
        en: "Entertainment ",
        es: "Entretenimiento"
    },
    "Farmacéutico": {
        pt: "Farmacêutico",
        en: "",
        es: "Farmacéutico"
    },
    "Financiero": {
        pt: "Financeiro",
        en: "Finances",
        es: "Financiero"
    },
    "Franquicias": {
        pt: "Franquias ",
        en: "Franchises",
        es: "Franquicias"
    },
    "Gobierno": {
        pt: "Governo ",
        en: "Goverment",
        es: "Gobierno"
    },
    "Herramientas": {
        pt: "Hotelaria ",
        en: "Haardware",
        es: "Herramientas"
    },
    "Hotelería": {
        pt: "Ferramentas",
        en: "Hotel",
        es: "Hotelería"
    },
    "Iluminación": {
        pt: "Iluminação ",
        en: "Lightning",
        es: "Iluminación"
    },
    "Ingredientes": {
        pt: "Ingredientes",
        en: "Ingredients",
        es: "Ingredientes"
    },
    "Medio Ambiente": {
        pt: "Meio Ambiente",
        en: "Enviromental",
        es: "Medio Ambiente"
    },
    "Minería y Metales": {
        pt: "Mineração e Metais",
        en: "Mining and Metals",
        es: "Minería y Metales"
    },
    "machinery_and_equipment": {
        pt: "Maquinário e equipamento",
        en: "Machinery and equipment",
        es: "Maquinaria y Equipo"
    },
    "Packaging": {
        pt: "Embalagem ",
        en: "Packaging",
        es: "Packaging"
    },
    "Petróleo y Gas": {
        pt: "Petróleo e Gás",
        en: "Oil and Gas",
        es: "Petróleo y Gas"
    },
    "Retail": {
        pt: "Retail",
        en: "Retail",
        es: "Retail"
    },
    "Salud": {
        pt: "Saúde ",
        en: "Healthcare",
        es: "Salud"
    },
    "Seguridad": {
        pt: "Segurança",
        en: "Safety and Security",
        es: "Seguridad"
    },
    "Servicios Públicos": {
        pt: "Serviços Públicos",
        en: "Public Services",
        es: "Servicios Públicos"
    },
    "Tecnología": {
        pt: "Tecnologia ",
        en: "Technology",
        es: "Tecnología"
    },
    "Textil": {
        pt: "Têxtil ",
        en: "Textiles",
        es: "Textil"
    },
    "Transporte": {
        pt: "Transporte ",
        en: "Transportation",
        es: "Transporte"
    },
    "Servicios financieros": {
        pt: "Serviços financeiros",
        en: "Financial Services",
        es: "Servicios financieros"
    },
    "Servicios profesionales": {
        pt: "Servicios profesionales",
        en: "Professional Services",
        es: "Servicios profesionales"
    },
    "Telecomunicaciones": {
        pt: "Telecomunicações",
        en: "Telecommunication",
        es: "Telecomunicaciones"
    },
    "Transporte y Logística": {
        pt: "Transporte e Logística",
        en: "Transportation and Logistic",
        es: "Transporte y Logística"
    },
    // Seccion 3
    "PAÍSES<br>DONDE OPERAMOS": {
        pt: "PAÍSES<br>ONDE OPERAMOS",
        en: "Countries <br> where we operate",
        es: "PAÍSES<br>DONDE OPERAMOS"
    },
    "Argentina": {
        pt: "Argentina",
        en: "Argentina",
        es: "Argentina"
    },
    "Bolivia": {
        pt: "Bolivia",
        en: "Bolivia",
        es: "Bolivia"
    },
    "Brasil": {
        pt: "Brasil",
        en: "Brazil",
        es: "Brasil"
    },
    "Chile": {
        pt: "Chile",
        en: "Chile",
        es: "Chile"
    },
    "Colombia": {
        pt: "Colômbia",
        en: "Colombia",
        es: "Colombia"
    },
    "Costa Rica": {
        pt: "Costa Rica",
        en: "",
        es: "Costa Rica"
    },
    "Ecuador": {
        pt: "Ecuador",
        en: "Ecuador",
        es: "Ecuador"
    },
    "Guatemala": {
        pt: "Guatemala",
        en: "Guatemala",
        es: "Guatemala"
    },
    "Honduras": {
        pt: "Honduras",
        en: "Honduras",
        es: "Honduras"
    },
    "México": {
        pt: "México",
        en: "Mexico",
        es: "México"
    },
    "Nicaragua": {
        pt: "Nicaragua",
        en: "Nicaragua",
        es: "Nicaragua"
    },
    "Panamá": {
        pt: "Panamá",
        en: "Panama",
        es: "Panamá"
    },
    "Paraguay": {
        pt: "Paraguai",
        en: "Paraguay",
        es: "Paraguay"
    },
    "Perú": {
        pt: "Perú",
        en: "Peru",
        es: "Perú"
    },
    "República Dominicana": {
        pt: "Rep. Dominicana",
        en: "Dominican Republic",
        es: "República Dominicana"
    },
    "Uruguay": {
        pt: "Uruguai",
        en: "Uruguay",
        es: "Uruguay"
    },
    // Formulario de contacto
    "CONTÁCTENOS": {
        pt: "Fale conosco",
        en: "Contact Us",
        es: "CONTÁCTENOS"
    },
    "Ayúdenos a comprender sus necesidades y cómo podríamos servirle.": {
        pt: "Nos ajude a compreender suas necesidades e como podemos serví-lo.",
        en: "Please help us understand your needs and how we can best help you",
        es: "Ayúdenos a comprender sus necesidades y cómo podríamos servirle."
    },
    "Nombre:": {
        pt: "Nome:",
        en: "First name:",
        es: "Nombre:"
    },
    "Apellido:": {
        pt: "Sobrenome:",
        en: "Last name:",
        es: "Apellido:"
    },
    "Empresa:": {
        pt: "Empresa:",
        en: "Company:",
        es: "Empresa:"
    },
    "País:": {
        pt: "País:",
        en: "Country:",
        es: "País:"
    },
    "Email:": {
        pt: "Email:",
        en: "Email:",
        es: "Email:"
    },
    "¿Cómo podemos ayudarlo?:": {
        pt: "Como podemos te ajudar?:",
        en: "How can we help you?:",
        es: "¿Cómo podemos ayudarlo?:"
    },
    "Enviar": {
        pt: "Enviar",
        en: "Submit",
        es: "Enviar"
    },
};

$(function () {
    // Inicializacion de variables
    let english = $('#en');
    let portuguese = $('#pt');
    let spanish = $('#es');

    // Ingles
    english.on('click', function (e) {
        e.preventDefault();
        let translator = $('body').translate({lang: "en", t: dict});
        localStorage.setItem('lang', 'en');

        // Obtener traducciones
        let name = translator.get("Nombre:");
        let apellido = translator.get("Apellido:");
        let empresa = translator.get("Empresa:");
        let pais = translator.get("País:");
        let email = translator.get("Email:");
        let ayuda = translator.get("¿Cómo podemos ayudarlo?:");

        // Reemplazar en el form
        $('#inputName').attr('placeholder', name);
        $('#inputLastname').attr('placeholder', apellido);
        $('#inputCompany').attr('placeholder', empresa);
        $('#inputCountry').attr('placeholder', pais);
        $('#inputEmail').attr('placeholder', email);
        $('#validationTextarea').attr('placeholder', ayuda);
    });

    // Spanish
    spanish.on('click', function (e) {
        e.preventDefault();
        let translator = $('body').translate({lang: "es", t: dict});
        localStorage.setItem('lang', 'es');

        // Obtener traducciones
        let name = translator.get("Nombre:");
        let apellido = translator.get("Apellido:");
        let empresa = translator.get("Empresa:");
        let pais = translator.get("País:");
        let email = translator.get("Email:");
        let ayuda = translator.get("¿Cómo podemos ayudarlo?:");

        // Reemplazar en el form
        $('#inputName').attr('placeholder', name);
        $('#inputLastname').attr('placeholder', apellido);
        $('#inputCompany').attr('placeholder', empresa);
        $('#inputCountry').attr('placeholder', pais);
        $('#inputEmail').attr('placeholder', email);
        $('#validationTextarea').attr('placeholder', ayuda);
    });

    // Portugues
    portuguese.on('click', function (e) {
        e.preventDefault();
        let translator = $('body').translate({lang: "pt", t: dict});
        localStorage.setItem('lang', 'pt');

        // Obtener traducciones
        let name = translator.get("Nombre:");
        let apellido = translator.get("Apellido:");
        let empresa = translator.get("Empresa:");
        let pais = translator.get("País:");
        let email = translator.get("Email:");
        let ayuda = translator.get("¿Cómo podemos ayudarlo?:");

        // Reemplazar en el form
        $('#inputName').attr('placeholder', name);
        $('#inputLastname').attr('placeholder', apellido);
        $('#inputCompany').attr('placeholder', empresa);
        $('#inputCountry').attr('placeholder', pais);
        $('#inputEmail').attr('placeholder', email);
        $('#validationTextarea').attr('placeholder', ayuda);
    });
    // Set defualt language
    const lang = localStorage.getItem('lang');
    if (lang === "en"){
        $( "#en" ).trigger( "click" );
    }
    if (lang === "es"){
        $( "#es" ).trigger( "click" );
    }
    if (lang === "pt"){
        $( "#pt" ).trigger( "click" );
    }
    if ( lang == null){
        $( "#en" ).trigger( "click" );
    }

});
